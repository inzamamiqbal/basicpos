﻿using basicPos2.models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace basicPos2
{
    /// <summary>
    /// Interaction logic for AddPayment.xaml
    /// </summary>
    public partial class AddPayment : Window
    {
        private Sales s;
        private Payment m;
        private ObservableCollection<Payment> list;
        private Salesuser p;
        public AddPayment(Salesuser p,Sales s, Payment m,ObservableCollection<Payment> list)
        {
            InitializeComponent();
            this.s = s;
            this.m = m;
            this.p = p;
            this.list = list;
            this.Date.SelectedDate = DateTime.Today;
            this.Time.SelectedTime = DateTime.Now;
            if (this.m != null)
            {
                try
                {
                    Date.SelectedDate = DateTime.ParseExact(m.Date, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                    Time.SelectedTime = Convert.ToDateTime(m.Time);
                }
                catch (Exception e)
                {
                    Date.Text = m.Date;
                }
                Reference.Text = m.Reference;
                PaymentMethod.Text = m.PaidBy;
                Amount.Text = m.Amount + "";
            }

        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            Date.BorderBrush = Brushes.Black;
            Amount.BorderBrush = Brushes.Black;
            Time.BorderBrush = Brushes.Black;

            if (Time.SelectedTime == null)
            {
                Time.BorderBrush = Brushes.Red;
                MessageBox.Show("Please select Time", "over");

            }
            else
            {
                if (Date.SelectedDate == null)
                {
                    Date.BorderBrush = Brushes.Red;
                    MessageBox.Show("Please select date", "over");
                }
                else
                {
                    if (PaymentMethod.SelectedIndex == 0)
                    {
                        try
                        {
                            float.Parse(Amount.Text);
                            if (this.m != null)
                            {if ((Math.Abs(s.Total) < Math.Abs(s.Paid - this.m.Amount + float.Parse(Amount.Text))) || float.Parse(Amount.Text) == 0)
                                {
                                    Amount.BorderBrush = Brushes.Red;
                                    MessageBox.Show("Please input a suitable amount");
                                }
                                else
                                {
                                    Payment.editPaymentinGivenList(this.m.Amount, list.IndexOf(this.m), new Payment { Time = Time.Text, Id = this.m.Id, Date = Date.Text, SaleID = this.m.SaleID, PaidBy = PaymentMethod.Text, Amount = float.Parse(Amount.Text), Reference = Reference.Text }, list);
                                    Sales.ClearAllSales();
                                    Sales.getFromDatabase();
                                    p.Total.Content = ((ObservableCollection<Sales>)p.produtsdataGrid.ItemsSource).Sum(item => item.Total);
                                    p.Discount.Content = ((ObservableCollection<Sales>)p.produtsdataGrid.ItemsSource).Sum(item => item.TotalDiscount);
                                    p.GrandTotal.Content = ((ObservableCollection<Sales>)p.produtsdataGrid.ItemsSource).Sum(item => item.GrandTotal);
                                    p.Paid.Content = ((ObservableCollection<Sales>)p.produtsdataGrid.ItemsSource).Sum(item => item.Paid);

                                    this.Close();
                                }
                            }
                            else
                            {
                                if ((Math.Abs(s.Total) < Math.Abs(s.Paid + float.Parse(Amount.Text))) || float.Parse(Amount.Text)==0)
                                {
                                    Amount.BorderBrush = Brushes.Red;
                                    MessageBox.Show("Please input a suitable amount");
                                }
                                else
                                {
                                    Payment.AddPayment(new Payment { Time = Time.Text, Id = Payment.AllPayment.Count + 1, Date = Date.Text, SaleID = this.s.Id, PaidBy = PaymentMethod.Text, Amount = float.Parse(Amount.Text), Reference = Reference.Text });
                                    Sales.ClearAllSales();
                                    Sales.getFromDatabase();

                                    p.Total.Content = ((ObservableCollection<Sales>)p.produtsdataGrid.ItemsSource).Sum(item => item.Total);
                                    p.Discount.Content = ((ObservableCollection<Sales>)p.produtsdataGrid.ItemsSource).Sum(item => item.TotalDiscount);
                                    p.GrandTotal.Content = ((ObservableCollection<Sales>)p.produtsdataGrid.ItemsSource).Sum(item => item.GrandTotal);
                                    p.Paid.Content = ((ObservableCollection<Sales>)p.produtsdataGrid.ItemsSource).Sum(item => item.Paid);

                                    this.Close();
                                }
                            }
                        }
                        catch
                        {
                            Amount.BorderBrush = Brushes.Red;
                            MessageBox.Show("Please input a valid amount");
                        }
                    }
                    else
                    {
                        if (Amount.Text.Equals(""))
                        {
                            Amount.BorderBrush = Brushes.Red;
                            MessageBox.Show("Please input a cheque Number");

                        }
                        else
                        {
                            //code for cheque

                        }
                    }
                }

            }
        }

        private void PaymentMethod_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (PaymentMethod.SelectedIndex == 1)
            {
                label6.Content = "Cheque No";
                Amount.Text = "";
            }else
            {
                if (label6 != null)
                {
                    label6.Content = "Amount";
                    Amount.Text = "0.00";
                }

            }
        }
    }
}
