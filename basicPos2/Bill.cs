﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using basicPos2.models;
using System.Collections.ObjectModel;
using basicPos2;

namespace basicPos2
{
    public partial class Bill : Form
    {
        List<SaleItem> _list = SaleItem.CurrentBill.ToList();
        Sales _sales = Sales.currentSale;
        float _total, _discount, _grandTotal;
        string _company, _address1, _address2;
        Pos _pos;
        bool _fromPos;
      

        public Bill(Pos pos, bool fromPos, Sales sale)
        {
            InitializeComponent();
            _fromPos = fromPos;
           
            if (fromPos)
            {
                _pos = pos;
            }
            else
            {
                _sales=Sales.getSalesItems(sale);
                _list = _sales.SaleItems.ToList();
                
            }

            _total = _sales.Total;
            _discount = _sales.TotalDiscount;
            _grandTotal = _sales.GrandTotal;
            _company = RealUtility.companyName;
            _address1 = RealUtility.address1;
            _address2 = RealUtility.address2;



        }

        private void Bill_Load(object sender, EventArgs e)
        {
            SaleItemBindingSource.DataSource = _list;

            Microsoft.Reporting.WinForms.ReportParameter[] para = new Microsoft.Reporting.WinForms.ReportParameter[]
            {

                new Microsoft.Reporting.WinForms.ReportParameter("total",string.Format("{0:N2}", _total)),
                new Microsoft.Reporting.WinForms.ReportParameter("totalDiscount", string.Format("{0:N2}", _discount)),
                new Microsoft.Reporting.WinForms.ReportParameter("grandTotal", string.Format("{0:N2}", _grandTotal)),
                new Microsoft.Reporting.WinForms.ReportParameter("address1", _address1) ,
                new Microsoft.Reporting.WinForms.ReportParameter("address2", _address2),
                new Microsoft.Reporting.WinForms.ReportParameter("companyName", _company)
            };


            this.reportViewer1.LocalReport.SetParameters(para);
            this.reportViewer1.RefreshReport();

            if (_fromPos)
            {
                updateDataFromPos();
            }

            
        }

       

        private void updateDataFromPos()
        {
            
            SaleItem._currentBill.Clear();
            Sales.currentSale = new Sales { Date = DateTime.Today.ToString("YYYY-MM-DD"), SaleItems = SaleItem.CurrentBill, CustomerId = 1, CustomerName = Customer.AllCustomers[0].Name };
            _pos.customerCombo.SelectedItem = _pos.customerCombo.Items[_pos.customerCombo.Items.Count - 1];
            _pos.updateBillDetails();
        }
        
    }
}
