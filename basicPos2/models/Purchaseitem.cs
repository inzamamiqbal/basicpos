﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace basicPos2.models
{
    public partial class PurchaseItem : INotifyPropertyChanged
    {
        int _id;
        int _purchase_id;
        int _product_id;
        float _quantity;
        float _cost;
        float _subtotal;
        string _name;
        


        //string _image;
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
                RaisePropertyChanged("Name");
            }
        }

        public int Id
        {
            get
            {
                return _id;
            }

            set
            {
                _id = value;
                RaisePropertyChanged("Id");
            }
        }

        public int PurchaseID
        {
            get
            {
                return _purchase_id;
            }

            set
            {
                _purchase_id = value;
                RaisePropertyChanged("PurchaseID");
            }
        }

        public int ProductID
        {
            get
            {
                return _product_id;
            }

            set
            {
                _product_id = value;
                RaisePropertyChanged("ProductID");
            }
        }
        public float Quantity
        {
            get
            {
                return _quantity;
            }

            set
            {
                _quantity = value;
                RaisePropertyChanged("Quantity");
                RaisePropertyChanged("Subtotal");
            }
        }
        public float Cost
        {
            get
            {
                return _cost;
            }

            set
            {
                _cost = value;
                RaisePropertyChanged("Cost");
                RaisePropertyChanged("Subtotal");
            }
        }

        public float Subtotal
        {
            get
            {
                return _quantity*_cost;
            }

            set
            {
                _subtotal = value;
                RaisePropertyChanged("Subtotal");
            }
        }
        private void RaisePropertyChanged(string propertyName)
        {

            if (null != PropertyChanged)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
        public event PropertyChangedEventHandler PropertyChanged;
    }
    }
