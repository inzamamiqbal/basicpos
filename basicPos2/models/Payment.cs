﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace basicPos2.models
{
    public partial class Payment : INotifyPropertyChanged
    {
        int _id;
        int _saleid;
        string _date;
        string _time;
        string _reference;
        float _amount;
        string _paidby;
        string _datedis;


        //string _image;


        public int Id
        {
            get
            {
                return _id;
            }

            set
            {
                _id = value;
                RaisePropertyChanged("Id");
            }
        }

        public int SaleID
        {
            get
            {
                return _saleid;
            }

            set
            {
                _saleid = value;
                RaisePropertyChanged("SaleID");
            }
        }

        public string Date
        {
            get
            {
                return _date;
            }

            set
            {
                _date = value;
                RaisePropertyChanged("Date");
                RaisePropertyChanged("Datedis");

            }
        }
        public string Datedis
        {
            get
            {
                return _date+" "+_time;
            }

            set
            {
                
                RaisePropertyChanged("Datedis");
            }
        }
        public string Time
        {
            get
            {
                return _time;
            }

            set
            {
                _time = value;
                RaisePropertyChanged("Time");

                RaisePropertyChanged("Datedis");
            }
        }
        public float Amount
        {
            get
            {
                return _amount;
            }

            set
            {
                _amount = value;
                RaisePropertyChanged("Amount");
            }
        }
        public string Reference
        {
            get
            {
                return _reference;
            }

            set
            {
                _reference = value;
                RaisePropertyChanged("Reference");
            }
        }

        public string PaidBy
        {
            get
            {
                return _paidby;
            }

            set
            {
                _paidby = value;
                RaisePropertyChanged("PaidBy");
            }
        }




        /*public string Image
        {
            get
            {
                return _image;
            }

            set
            {
                _image = value;
                RaisePropertyChanged("Image");
            }
        }*/




        private void RaisePropertyChanged(string propertyName)
        {

            if (null != PropertyChanged)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
        public event PropertyChangedEventHandler PropertyChanged;

    }


    partial class Payment
    {
        static ObservableCollection<Payment> _needepayments = new ObservableCollection<Payment>();


        public static ObservableCollection<Payment> AllPayment
        {
            get
            {
                _needepayments.Clear();
                MySqlConnection connection = new MySqlConnection(RealUtility.dbString);

                try
                {
                    connection.Open();
                    string sql = "SELECT *,Date_format(date,'%d/%m/%Y') as x FROM tec_payments";
                    using (var command = new MySqlCommand(sql, connection))
                    {
                        using (var reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var product = new Payment();
                                product.Id = int.Parse(reader["id"].ToString());
                                product.SaleID = int.Parse(reader["sale_id"].ToString());
                                product.Reference = reader["reference"].ToString();
                                product.Date = reader["x"].ToString();
                                product.Amount = float.Parse(reader["amount"].ToString());
                                product.PaidBy = reader["paid_by"].ToString();




                                _needepayments.Insert(0,product);
                            }

                        }
                    }

                }
                catch (Exception)
                {
                    MessageBox.Show("Failed to Load from database", "Error-Payments");
                }

                finally
                {
                    if (connection.State == System.Data.ConnectionState.Open)
                    {
                        connection.Close();
                    }
                }

                return _needepayments;
            }
        }
        public static void DeleteFromAllGivenPayment(Payment item, ObservableCollection<Payment> givenlist)
        {
            MySqlConnection connection = new MySqlConnection(RealUtility.dbString);

            try
            {

                connection.Open();
                MySqlCommand cmd;
                cmd = connection.CreateCommand();
                cmd.CommandText = "DELETE FROM tec_payments WHERE id=" + item.Id;
                cmd.ExecuteNonQuery();
                cmd.CommandText = "UPDATE tec_sales set paid=paid-" + item.Amount + " where id=" + item.SaleID;
                cmd.ExecuteNonQuery();
                givenlist.Remove(item);

            }
            catch (Exception)
            {
                MessageBox.Show("Failed to delete", "Error");
            }

            finally
            {
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    connection.Close();
                }
            }

        }
        public static void editPaymentinGivenList(float i, int j, Payment item, ObservableCollection<Payment> givenlist)
        {
            MySqlConnection connection = new MySqlConnection(RealUtility.dbString);

            try
            {

                connection.Open();
                MySqlCommand cmd;
                cmd = connection.CreateCommand();
                cmd.CommandText = "UPDATE tec_payments set date='"+ Convert.ToDateTime(item.Date).ToString("yyyy/MM/dd") +" "+ RealUtility.convto24(item.Time) + "', paid_by='" +item.PaidBy+"', amount='"+item.Amount+"', reference='"+item.Reference+"' WHERE id="+item.Id;

                cmd.ExecuteNonQuery();
                cmd.CommandText = "UPDATE tec_sales set paid=paid-" + i + "+" + item.Amount + " where id=" + item.SaleID;
                cmd.ExecuteNonQuery();
                if (givenlist != null)
                {
                    givenlist.RemoveAt(j);
                    givenlist.Insert(j, item);
                }

            }
            catch (Exception)
            {
                MessageBox.Show("Failed to edit", "Error");
            }

            finally
            {
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    connection.Close();
                }
            }
        }
        public static void AddPayment(Payment m)
        {
            MySqlConnection connection = new MySqlConnection(RealUtility.dbString);

            try
            {

                connection.Open();
                MySqlCommand cmd;
                cmd = connection.CreateCommand();
                cmd.CommandText = "Insert into tec_payments(id,date,sale_id,paid_by,amount,reference) Values ('" + m.Id +  "','" + Convert.ToDateTime(m.Date).ToString("yyyy/MM/dd")+" "+ RealUtility.convto24(m.Time) + "','"+m.SaleID + "','" + m.PaidBy + "','" +m.Amount + "','" + m.Reference + "')";

                cmd.ExecuteNonQuery();
                cmd.CommandText = "UPDATE tec_sales set paid=paid+" + m.Amount + " where id=" + m.SaleID;
                cmd.ExecuteNonQuery();


            }
            catch (Exception)
            {
                MessageBox.Show("Failed to edit", "Error");
            }

            finally
            {
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    connection.Close();
                }
            }
        }


        public static void getFromDatabase(Sales item, ObservableCollection<Payment> givenlist)
        {

            MySqlConnection connection = new MySqlConnection(RealUtility.dbString);

            try
            {
                connection.Open();
                string sql = "SELECT *,Date_format(date,'%m/%d/%Y') as x,time_format(date , '%r') as y FROM tec_payments WHERE sale_id=" + item.Id;

                using (var command = new MySqlCommand(sql, connection))
                {
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var product = new Payment();
                            product.Id = int.Parse(reader["id"].ToString());
                            product.SaleID = int.Parse(reader["sale_id"].ToString());
                            product.Reference = reader["reference"].ToString();
                            product.Date = reader["x"].ToString();
                            product.Time = reader["y"].ToString();
                            product.Amount = float.Parse(reader["amount"].ToString());
                            product.PaidBy = reader["paid_by"].ToString();




                            givenlist.Insert(0,product);
                        }

                    }
                }

            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString(), "Error");
            }

            finally
            {
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    connection.Close();
                }
            }
        }

    }
}
