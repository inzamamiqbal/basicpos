﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace basicPos2.models
{
    public partial class User : INotifyPropertyChanged
    {
        int _id;
        int _level;
        string _username;
        string _password;
        string _group_name;
        string _gender;
        string _phone;
        string _status;
        string _email;
        


        //string _image;


        public int Id
        {
            get
            {
                return _id;
            }

            set
            {
                _id = value;
                RaisePropertyChanged("Id");
            }
        }
        public int Level
        {
            get
            {
                return _level;
            }

            set
            {
                _level = value;
                RaisePropertyChanged("Level");
            }
        }

        public string Name
        {
            get
            {
                return _username;
            }

            set
            {
                _username = value;
                RaisePropertyChanged("Name");
            }
        }

        public string Password
        {
            get
            {
                return _password;
            }

            set
            {
                _password = value;
                RaisePropertyChanged("Password");
            }
        }
        public string Group
        {
            get
            {
                return _group_name;
            }

            set
            {
                _group_name = value;
                RaisePropertyChanged("Group");
            }
        }
        public string Phone
        {
            get
            {
                return _phone;
            }

            set
            {
                _phone = value;
                RaisePropertyChanged("Phone");
            }
        }

        public string Gender
        {
            get
            {
                return _gender;
            }

            set
            {
                _gender = value;
                RaisePropertyChanged("Gender");
            }
        }

        public string Status
        {
            get
            {
                return _status;
            }

            set
            {
                _status = value;
                RaisePropertyChanged("Status");
            }
        }
        public string Email
        {
            get
            {
                return _email;
            }

            set
            {
                _email = value;
                RaisePropertyChanged("Email");
            }
        }

        /*public string Image
        {
            get
            {
                return _image;
            }

            set
            {
                _image = value;
                RaisePropertyChanged("Image");
            }
        }*/




        private void RaisePropertyChanged(string propertyName)
        {

            if (null != PropertyChanged)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
        public event PropertyChangedEventHandler PropertyChanged;

    }


    partial class User
    {
        static ObservableCollection<User> _alluser= new ObservableCollection<User>();


        public static ObservableCollection<User> AllUser
        {
            get
            {
                if (_alluser.Count == 0)
                {

                    getFromDatabase();
                }

                return _alluser;
            }
        }

        public static void AddToAllUsers(User item)
        {


            MySqlConnection connection = new MySqlConnection(RealUtility.dbString);

            try
            {

                connection.Open();
                MySqlCommand cmd;
                cmd = connection.CreateCommand();
                cmd.CommandText = "Insert into tec_users(id,username,email,phone,password,level,gender,active,group_name) Values ('" + item.Id + "','" + item.Name + "','" + item.Email + "','" + item.Phone + "','" + item.Password + "','" + item.Level + "','" + item.Gender + "','" + item.Status + "','"+ item.Group + "')";
                cmd.ExecuteNonQuery();
                _alluser.Insert(0,item);

            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString(), "Error");
            }

            finally
            {
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    connection.Close();
                }
            }

        }

        public static void DeleteFromAllUsers(User item)
        {
            MySqlConnection connection = new MySqlConnection(RealUtility.dbString);

            try
            {

                connection.Open();
                MySqlCommand cmd;
                cmd = connection.CreateCommand();
                cmd.CommandText = "DELETE FROM tec_users WHERE id=" + item.Id;
                cmd.ExecuteNonQuery();
                _alluser.Remove(item);

            }
            catch (Exception)
            {
                MessageBox.Show("Failed to delete", "Error");
            }

            finally
            {
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    connection.Close();
                }
            }

        }
        public static void editUser(int i, User item)
        {
            MySqlConnection connection = new MySqlConnection(RealUtility.dbString);

            try
            {

                connection.Open();
                MySqlCommand cmd;
                cmd = connection.CreateCommand();
                cmd.CommandText = "DELETE FROM tec_users WHERE id=" + item.Id;
                cmd.ExecuteNonQuery();
                cmd.CommandText = "Insert into tec_users(id,username,email,phone,password,level,gender,active,group_name) Values ('" + item.Id + "','" + item.Name + "','" + item.Email + "','" + item.Phone + "','" + item.Password + "','" + item.Level + "','" + item.Gender + "','" + item.Status + "','" + item.Group + "')";
                cmd.ExecuteNonQuery();
                _alluser.RemoveAt(i);
                _alluser.Insert(i, item);

            }
            catch (Exception)
            {
                MessageBox.Show("Failed to edit", "Error");
            }

            finally
            {
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    connection.Close();
                }
            }
        }

        public static void ClearAllSuppliers()
        {
            _alluser.Clear();
        }

        public override string ToString()
        {
            return Name + " (" + Phone + " )";

        }
        public static void getFromDatabase()
        {

            MySqlConnection connection = new MySqlConnection(RealUtility.dbString);

            try
            {
                connection.Open();
                string sql = "SELECT * FROM tec_users";
                using (var command = new MySqlCommand(sql, connection))
                {
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var product = new User();
                            product.Id = int.Parse(reader["id"].ToString());
                            product.Name = reader["username"].ToString();
                            product.Password = reader["password"].ToString();
                            product.Gender = reader["gender"].ToString();
                            product.Phone = reader["phone"].ToString();
                            product.Email = reader["email"].ToString();
                            product.Status = reader["active"].ToString();
                            product.Group = reader["group_name"].ToString();
                            product.Level =int.Parse(reader["level"].ToString());




                            _alluser.Insert(0,product);
                        }

                    }
                }

            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString(), "Error-Users");
            }

            finally
            {
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    connection.Close();
                }
            }
        }
    }
}
