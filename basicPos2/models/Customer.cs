﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Collections.ObjectModel;
using MySql.Data.MySqlClient;
using System.Windows;

namespace basicPos2.models
{
    public partial class Customer:INotifyPropertyChanged
    {
        int _id;
        string _name;
        string _phone;
        string _email;
        string _cf1;
        string _cf2;

        public int Id
        {
            get
            {
                return _id;
            }

            set
            {
                _id = value;
                RaisePropertyChanged("Id");
            }
        }

        public string Name
        {
            get
            {
                return _name;
            }

            set
            {
                _name = value;
                RaisePropertyChanged("Name");
            }
        }

        public string Phone
        {
            get
            {
                return _phone;
            }

            set
            {
                _phone = value;
                RaisePropertyChanged("Phone");
            }
        }

        public string Email
        {
            get
            {
                return _email;
            }

            set
            {
                _email = value;
                RaisePropertyChanged("Email");
            }
        }

        public string Cf1
        {
            get
            {
                return _cf1;
            }

            set
            {
                _cf1 = value;
                RaisePropertyChanged("Cf1");
            }
        }

        public string Cf2
        {
            get
            {
                return _cf2;
            }

            set
            {
                _cf2 = value;
                RaisePropertyChanged("Cf2");
            }
        }

        private void RaisePropertyChanged(string propertyName)
        {

            if (null != PropertyChanged)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
        public event PropertyChangedEventHandler PropertyChanged;
    }

    public partial class Customer
    {
        static ObservableCollection<Customer> _allsuppliers = new ObservableCollection<Customer>();


        public static ObservableCollection<Customer> AllCustomers
        {
            get
            {
                if (_allsuppliers.Count == 0)
                {

                    getFromDatabase();
                }

                return _allsuppliers;
            }
        }

        public static void AddToAllCustomers(Customer item)
        {


            MySqlConnection connection = new MySqlConnection(RealUtility.dbString);

            try
            {

                connection.Open();
                MySqlCommand cmd;
                cmd = connection.CreateCommand();
                cmd.CommandText = "Insert into tec_customers(id,name,cf1,cf2,phone,email) Values ('" + item.Id + "','" + item.Name + "','" + item.Cf1 + "','" + item.Cf2 + "','" + item.Phone + "','" + item.Email + "')";
                cmd.ExecuteNonQuery();
                _allsuppliers.Insert(0,item);

            }
            catch (Exception)
            {
                MessageBox.Show("Failed to add to Supplier", "Error");
            }

            finally
            {
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    connection.Close();
                }
            }

        }

        public static void DeleteFromAllCustomers(Customer item)
        {
            MySqlConnection connection = new MySqlConnection(RealUtility.dbString);

            try
            {

                connection.Open();
                MySqlCommand cmd;
                cmd = connection.CreateCommand();
                cmd.CommandText = "DELETE FROM tec_customers WHERE id=" + item.Id;
                cmd.ExecuteNonQuery();
                _allsuppliers.Remove(item);

            }
            catch (Exception)
            {
                MessageBox.Show("Failed to delete", "Error");
            }

            finally
            {
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    connection.Close();
                }
            }

        }
        public static void editCustomer(int i, Customer item)
        {
            MySqlConnection connection = new MySqlConnection(RealUtility.dbString);

            try
            {

                connection.Open();
                MySqlCommand cmd;
                cmd = connection.CreateCommand();
                cmd.CommandText = "DELETE FROM tec_customers WHERE id=" + item.Id;
                cmd.ExecuteNonQuery();
                cmd.CommandText = "Insert into tec_customers(id,name,cf1,cf2,phone,email) Values ('" + item.Id + "','" + item.Name + "','" + item.Cf1 + "','" + item.Cf2 + "','" + item.Phone + "','" + item.Email + "')";
                cmd.ExecuteNonQuery();
                _allsuppliers.RemoveAt(i);
               _allsuppliers.Insert(i, item);

            }
            catch (Exception)
            {
                MessageBox.Show("Failed to edit", "Error");
            }

            finally
            {
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    connection.Close();
                }
            }
        }

        public static void ClearAllSuppliers()
        {
            _allsuppliers.Clear();
        }

        public override string ToString()
        {
            return Name + " (" + Phone + " )";

        }
        public static void getFromDatabase()
        {

            MySqlConnection connection = new MySqlConnection(RealUtility.dbString);

            try
            {
                connection.Open();
                string sql = "SELECT * FROM tec_customers";
                using (var command = new MySqlCommand(sql, connection))
                {
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var product = new Customer();
                            product.Id = int.Parse(reader["id"].ToString());
                            product.Name = reader["name"].ToString();
                            product.Cf1 = reader["cf1"].ToString();
                            product.Cf2 = reader["cf2"].ToString();
                            product.Phone = reader["phone"].ToString();
                            product.Email = reader["email"].ToString();




                            _allsuppliers.Insert(0,product);
                        }

                    }
                }

            }
            catch (Exception)
            {
                MessageBox.Show("Failed to Load from database", "Error-Customer");
            }

            finally
            {
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    connection.Close();
                }
            }
        }
    }
}
