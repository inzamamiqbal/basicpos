﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace basicPos2.models
{
    public partial class Register
    {
        int _id;
        string _date;
        int _userId;
        float _cashInHand;
        string _status;
        float _totalCash;
        int _totalCheques;
        string _note;
        string _closedAt;
        int _closedBy;
        string _machineName;
        float _cashSalesTotal;
        float _noOfCheques;
        float _totalExpenses;

public int Id
        {
            get
            {
                return _id;
            }

            set
            {
                _id = value;
            }
        }

        public string Date
        {
            get
            {
                return _date;
            }

            set
            {
                _date = value;
            }
        }

        public int UserId
        {
            get
            {
                return _userId;
            }

            set
            {
                _userId = value;
            }
        }

        public float CashInHand
        {
            get
            {
                return _cashInHand;
            }

            set
            {
                _cashInHand = value;
            }
        }

        public string Status
        {
            get
            {
                return _status;
            }

            set
            {
                _status = value;
            }
        }

        public float TotalCash
        {
            get
            {
                return _totalCash;
            }

            set
            {
                _totalCash = value;
            }
        }

        public int TotalCheques
        {
            get
            {
                return _totalCheques;
            }

            set
            {
                _totalCheques = value;
            }
        }

        public string Note
        {
            get
            {
                return _note;
            }

            set
            {
                _note = value;
            }
        }

        public string ClosedAt
        {
            get
            {
                return _closedAt;
            }

            set
            {
                _closedAt = value;
            }
        }

        public int ClosedBy
        {
            get
            {
                return _closedBy;
            }

            set
            {
                _closedBy = value;
            }
        }

        public string MachineName
        {
            get
            {
                return _machineName;
            }

            set
            {
                _machineName = value;
            }
        }

        public float CashSalesTotal
        {
            get
            {
                return _cashSalesTotal;
            }

            set
            {
                _cashSalesTotal = value;
            }
        }

        public float NoOfCheques
        {
            get
            {
                return _noOfCheques;
            }

            set
            {
                _noOfCheques = value;
            }
        }

        public float TotalExpenses
        {
            get
            {
                return _totalExpenses;
            }

            set
            {
                _totalExpenses = value;
            }
        }
    }

    public partial class Register
    {
        public static Register currentRegister = new Register { CashSalesTotal = 0, NoOfCheques = 0, TotalExpenses = 0 };

        public static void getFromDb()
        {
            MySqlConnection connection = new MySqlConnection(RealUtility.dbString);

            try
            {
                connection.Open();

                string sql = "select * from tec_registers where status='open' and machine_name='" + System.Environment.MachineName + "'";
                using (var command = new MySqlCommand(sql, connection))
                {
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            if (Convert.ToInt32(reader["id"].ToString()) != 0)
                            {
                                currentRegister.Id = Convert.ToInt32(reader["id"].ToString());
                                currentRegister.Date = reader["date"].ToString();
                                currentRegister.UserId = Convert.ToInt32(reader["user_id"].ToString());
                                currentRegister.CashInHand = float.Parse(reader["cash_in_hand"].ToString());
                                currentRegister.Status = reader["status"].ToString();
                                currentRegister.MachineName = reader["machine_name"].ToString();
                                RealUtility.isRegisterOn = 1;
                            }
                            
                                                      
                        }


                    }
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                MessageBox.Show("Couldn't connect to database", "Connection Error");
                return;
            }

            finally
            {
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    connection.Close();
                }
            }
        }

        public static void closeRegister()
        {
            updateFromDb();

            MySqlConnection connection = new MySqlConnection(RealUtility.dbString);

            try
            {
                connection.Open();

                string sql = "update tec_registers set status='close', total_cash='"+currentRegister.TotalCash+"', total_cheques='"+currentRegister.NoOfCheques+"', closed_at='"+ DateTime.Now.ToString("yyyy-MM-dd HH:mm") + "', closed_by='"+RealUtility.currentUserId+"' where id='"+currentRegister.Id+"'";

                MySqlCommand cmd;
                cmd = connection.CreateCommand();
                cmd.CommandText = sql;

                cmd.ExecuteNonQuery();
                RealUtility.isRegisterOn = 0;

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                MessageBox.Show("Couldn't connect to database", "Connection Error");
                return;
            }

            finally
            {
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    connection.Close();
                }
            }


        }

        public static void updateFromDb()
        {
            MySqlConnection connection = new MySqlConnection(RealUtility.dbString);

            try
            {
                connection.Open();

                string sql = "select sum(amount) as cash, (select sum(amount) from tec_payments where date>'"+currentRegister.Date+ "' and paid_by='Cheque') as cheque, (select sum(amount) from tec_expenses where date>'" + currentRegister.Date+ "' ) as expenses from tec_payments where date>'" + currentRegister.Date+ "' and paid_by='Cash' ";
                Console.WriteLine(sql);
                using (var command = new MySqlCommand(sql, connection))
                {
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            currentRegister.CashSalesTotal = float.Parse(reader["cash"].ToString());
                            currentRegister.NoOfCheques = float.Parse(reader["cheque"].ToString());
                            currentRegister.TotalExpenses = float.Parse(reader["expenses"].ToString());
                            
                        }


                    }
                }

            }
            catch (MySqlException e)
            {
                Console.WriteLine(e.Message);
                MessageBox.Show(e.Message, "Connection Error");
                return;
            }catch(Exception e)
            {

            }

            finally
            {
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    connection.Close();
                }
            }


        }

        
    }
}
