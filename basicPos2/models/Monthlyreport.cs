﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace basicPos2.models
{
    public partial class MonthlyReport : INotifyPropertyChanged
    {

        string _date;
        float _total;
        float _total_discount;
        float _grand_total;
        float _total_paid;



        //string _image;




        public string Date
        {
            get
            {
                return _date;
            }

            set
            {
                _date = value;
                RaisePropertyChanged("Date");
            }
        }

        public float Total
        {
            get
            {
                return _total;
            }

            set
            {
                _total = value;
                RaisePropertyChanged("Total");

            }
        }
        public float TotalDiscount
        {
            get
            {
                return _total_discount;
            }

            set
            {
                _total_discount = value;
                RaisePropertyChanged("TotalDiscount");
            }
        }
        public float GrandTotal
        {
            get
            {
                return _grand_total;
            }

            set
            {
                _grand_total = value;
                RaisePropertyChanged("GrandTotal");
            }
        }
        public float TotalPaid
        {
            get
            {
                return _total_paid;
            }

            set
            {
                _total_paid = value;
                RaisePropertyChanged("TotalPaid");
            }
        }
        private void RaisePropertyChanged(string propertyName)
        {

            if (null != PropertyChanged)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
        public event PropertyChangedEventHandler PropertyChanged;

    }


    partial class MonthlyReport
    {
        static ObservableCollection<MonthlyReport> _monthlyreports = new ObservableCollection<MonthlyReport>();


        public static ObservableCollection<MonthlyReport> Monthlyreport(string datestart, string dateend)
        {

            _monthlyreports.Clear();
            MySqlConnection connection = new MySqlConnection(RealUtility.dbString);

            try
            {
                connection.Open();
                string sql = "SELECT Date_format(date,'%m/%Y') as datedis,sum(total) as totalday,sum(total_discount) as totdis,sum(grand_total) as grandtotalday,sum(paid) as totalpaid FROM `tec_sales` where date >= '" + Convert.ToDateTime(datestart).ToString("yyyy/MM/dd") + "' and date<='" + Convert.ToDateTime(dateend).ToString("yyyy/MM/dd") + " 23:59:59' group by datedis";
                using (var command = new MySqlCommand(sql, connection))
                {
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var product = new MonthlyReport();
                            product.Date = reader["datedis"].ToString();
                            product.Total = float.Parse(reader["totalday"].ToString());
                            product.TotalDiscount = float.Parse(reader["totdis"].ToString());
                            product.GrandTotal = float.Parse(reader["grandtotalday"].ToString());
                            product.TotalPaid = float.Parse(reader["totalpaid"].ToString());
                            _monthlyreports.Insert(0,product);
                        }

                    }
                }

            }
            catch (Exception e)
            {
                MessageBox.Show("Failed to Load from database", "Error-Monthly Sales Report");
            }

            finally
            {
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    connection.Close();
                }
            }

            return _monthlyreports;
        }
        static ObservableCollection<MonthlyReport> _monthlypurchasereports = new ObservableCollection<MonthlyReport>();


        public static ObservableCollection<MonthlyReport> MonthlyPurchasereport(string datestart, string dateend)
        {

            _monthlypurchasereports.Clear();
            MySqlConnection connection = new MySqlConnection(RealUtility.dbString);

            try
            {
                connection.Open();
                string sql = "SELECT Date_format(date,'%m/%Y') as datedis,sum(total) as totalday from `tec_purchases` where date >= '" + Convert.ToDateTime(datestart).ToString("yyyy/MM/dd") + "' and date<='" + Convert.ToDateTime(dateend).ToString("yyyy/MM/dd") + " 23:59:59' group by datedis";
                using (var command = new MySqlCommand(sql, connection))
                {
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var product = new MonthlyReport();
                            product.Date = reader["datedis"].ToString();
                            product.Total = float.Parse(reader["totalday"].ToString());
                            _monthlypurchasereports.Insert(0,product);
                        }

                    }
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                MessageBox.Show("Failed to Load from database", "Error-Monthly Purchase Report");
            }

            finally
            {
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    connection.Close();
                }
            }

            return _monthlypurchasereports;
        }
        static ObservableCollection<MonthlyReport> _monthlyexpensereports = new ObservableCollection<MonthlyReport>();


        public static ObservableCollection<MonthlyReport> Monthlyexpensereport(string datestart, string dateend)
        {

            _monthlyexpensereports.Clear();
            MySqlConnection connection = new MySqlConnection(RealUtility.dbString);

            try
            {
                connection.Open();
                string sql = "SELECT Date_format(date,'%m/%Y') as datedis,sum(amount) as totalday FROM `tec_expenses` where date >= '" + Convert.ToDateTime(datestart).ToString("yyyy/MM/dd") + "' and date<='" + Convert.ToDateTime(dateend).ToString("yyyy/MM/dd") + " 23:59:59' group by datedis";
                using (var command = new MySqlCommand(sql, connection))
                {
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var product = new MonthlyReport();
                            product.Date = reader["datedis"].ToString();
                            product.Total = float.Parse(reader["totalday"].ToString());
                            _monthlyexpensereports.Insert(0,product);
                        }

                    }
                }

            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString(), "Error");
            }

            finally
            {
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    connection.Close();
                }
            }

            return _monthlyexpensereports;
        }
    }

}

