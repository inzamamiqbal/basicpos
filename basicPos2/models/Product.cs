﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Collections.ObjectModel;
using MySql.Data.MySqlClient;
using System.Configuration;
using System.Windows;
using basicPos2.models;
using System.Windows.Media.Imaging;
using System.IO;
using System.Windows.Media;

namespace basicPos2.models
{
    public partial class Product:INotifyPropertyChanged
    {
        int _id;
        string _code;
        string _name;
        int _catagoryId;
        string _catagory;
        float _price;
        byte[] _image;
        string _vis;
        float _cost;
        float _quantity;
        ImageSource _imagei;
        string _type;
        string _details;
        float _alertQuantity;

        public int Id
        {
            get
            {
                return _id;
            }

            set
            {
                _id = value;
                RaisePropertyChanged("Id");
            }
        }
        public ImageSource Imagei
        {
            get
            {
                return _imagei;
            }

            set
            {
                _imagei = value;
                RaisePropertyChanged("Imagei");
            }
        }
        public int CatagoryId
        {
            get
            {
                return _catagoryId;
            }

            set
            {
                _catagoryId = value;
                RaisePropertyChanged("CatagoryId");
            }
        }
        public byte[] Image
        {
            get
            {
                return _image;
            }

            set
            {
                _image = value;
                RaisePropertyChanged("Image");
            }
        }
        public string Vis
        {
            get
            {
                return _vis;
            }
            set
            {
                _vis = value;
                RaisePropertyChanged("Vis");
            }
        }

        public string Code
        {
            get
            {
                return _code;
            }

            set
            {
                _code = value.ToUpper();
                RaisePropertyChanged("Code");
            }
        }

        public string Name
        {
            get
            {
                return _name;
            }

            set
            {
                _name = value.ToUpper();
                RaisePropertyChanged("Name");
            }
        }

       
        public float Price
        {
            get
            {
                return _price;
            }

            set
            {
                _price = value;
                RaisePropertyChanged("Price");
            }
        }

        

        public float Cost
        {
            get
            {
                return _cost;
            }

            set
            {
                _cost = value;
                RaisePropertyChanged("Cost");
            }
        }

        public float Quantity
        {
            get
            {
                return _quantity;
            }

            set
            {
                _quantity = value;
                RaisePropertyChanged("Quantity");
            }
        }

       

        public string Details
        {
            get
            {
                return _details;
            }

            set
            {
                _details = value;
                RaisePropertyChanged("Details");
            }
        }

        public float AlertQuantity
        {
            get
            {
                return _alertQuantity;
            }

            set
            {
                _alertQuantity = value;
                RaisePropertyChanged("AlertQuantity");
            }
        }

        public string Catagory
        {
            get
            {
                return (basicPos2.models.Catagory.AllCatagories.First(item => item.Id == _catagoryId)).Name;
                
            }

            set
            {
                _catagory = value;
                    RaisePropertyChanged("Category");
            }
        }

        public string Type
        {
            get
            {
                return _type;
            }

            set
            {
                _type = value;
            }
        }

        private void RaisePropertyChanged(string propertyName)
        {

            if (null != PropertyChanged)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
        public event PropertyChangedEventHandler PropertyChanged;

    }

    public enum Type
        {
           
        }

    partial class Product
    {
        static ObservableCollection<Product> _allProdutcs = new ObservableCollection<Product>();


        public static ObservableCollection<Product> AllProducts
        {
            get {
                if (_allProdutcs.Count==0)
                {
          
                    getFromDatabase();
                }

                return _allProdutcs;
            }
        }

        public static void AddToAllProduts(Product item)
        {
            

            MySqlConnection connection = new MySqlConnection(RealUtility.dbString);
            
            try
            {
               
                connection.Open();
                MySqlCommand cmd;
                cmd = connection.CreateCommand();
                cmd.CommandText = "Insert into tec_products(id,code,name,price,cost,quantity,type,alert_quantity,details,category_id,image) Values ('" + item.Id + "','" + item.Code + "','" + item.Name + "','" + item.Price + "','" + item.Cost+ "','" + item.Quantity+ "','" + item.Type+ "','" + item.AlertQuantity + "','" + item.Details + "','" + item.CatagoryId + "',@image)";
                cmd.Parameters.Add("@image", MySqlDbType.LongBlob).Value = item.Image;
                cmd.ExecuteNonQuery();
                _allProdutcs.Insert(0,item);

            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString(), "Error");
            }

            finally
            {
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    connection.Close();
                }
            }

        }

        public static void DeleteFromAllProducts(Product item)
        {
            MySqlConnection connection = new MySqlConnection(RealUtility.dbString);

            try
            {
                
                connection.Open();
                MySqlCommand cmd;
                cmd = connection.CreateCommand();
                cmd.CommandText = "DELETE FROM tec_products WHERE id="+item.Id;
                cmd.ExecuteNonQuery();
                _allProdutcs.Remove(item);

            }
            catch (Exception)
            {
                MessageBox.Show("Failed to delete", "Error");
            }

            finally
            {
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    connection.Close();
                }
            }
            
        }
        public static void editProduct(int i,Product item)
        {
            MySqlConnection connection = new MySqlConnection(RealUtility.dbString);

            try
            {

                connection.Open();
                MySqlCommand cmd;
                cmd = connection.CreateCommand();
                cmd.CommandText = "DELETE FROM tec_products WHERE id=" + item.Id;
                cmd.ExecuteNonQuery();
                cmd.CommandText = "Insert into tec_products(id,code,name,price,cost,quantity,type,alert_quantity,details,category_id,image) Values ('" + item.Id + "','" + item.Code + "','" + item.Name + "','" + item.Price + "','" + item.Cost + "','" + item.Quantity + "','" + item.Type + "','" + item.AlertQuantity + "','" + item.Details + "','" + item.CatagoryId + "',@image)";
                cmd.Parameters.Add("@image", MySqlDbType.LongBlob).Value = item.Image;
                cmd.ExecuteNonQuery();


                _allProdutcs.RemoveAt(i);
                _allProdutcs.Insert(i, item);

            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString(), "Error");
            }

            finally
            {
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    connection.Close();
                }
            }
        }
        /*public static Byte[] ConvertToByteFromBitmapImage(BitmapImage bitmapImage)
        {
            byte[] data;
            JpegBitmapEncoder encoder = new JpegBitmapEncoder();

            encoder.Frames.Add(BitmapFrame.Create(bitmapImage));
            using (MemoryStream ms = new MemoryStream())
            {
                encoder.Save(ms);
                data = ms.ToArray();
            }

            return data;
        }*/

        public static void ClearAllProducts()
        {
            _allProdutcs.Clear();
        }

        public override string ToString()
        {
            return Name + " (" + Code + " )"; 
           
        }

        public static void addCombo(Product p,ObservableCollection<Product> children)
        {
            
            MySqlConnection connection = new MySqlConnection(RealUtility.dbString);

            for (int i = 0; i < children.Count(); i++)
            {
                p.Cost += children.ElementAt(i).Cost * children.ElementAt(i).Quantity;
                

            }
            Product.AddToAllProduts(p);
            for (int i = 0; i < children.Count(); i++)
            {
                try
                {

                    connection.Open();
                    MySqlCommand cmd;
                    cmd = connection.CreateCommand();
                    cmd.CommandText = "Insert into tec_combo_items(product_id,item_code,quantity) Values ('" +p.Id+"','"+children.ElementAt(i).Code+"','"+ children.ElementAt(i).Quantity+ "')";

                    cmd.ExecuteNonQuery();
                    


                }
                catch (Exception)
                {
                    MessageBox.Show("Failed to add to Combo Product", "Error");
                    break;
                }

                finally
                {
                    if (connection.State == System.Data.ConnectionState.Open)
                    {
                        
                        connection.Close();
                    }
                }
            }
            

        }
        public static void editCombo(Product initial,Product p,ObservableCollection<Product> children)
        {
            




            MySqlConnection connection = new MySqlConnection(RealUtility.dbString);
            try
            {

                connection.Open();
                MySqlCommand cmd;
                cmd = connection.CreateCommand();
                cmd.CommandText = "DELETE FROM tec_products WHERE id=" + initial.Id;
                cmd.ExecuteNonQuery();
                cmd.CommandText = "Insert into tec_products(id,code,name,price,cost,quantity,type,alert_quantity,details,category_id,image) Values ('" + p.Id + "','" + p.Code + "','" + p.Name + "','" + p.Price + "','" + p.Cost + "','" + p.Quantity + "','" + p.Type + "','" + p.AlertQuantity + "','" + p.Details + "','" + p.CatagoryId + "',@image)";
                cmd.Parameters.Add("@image", MySqlDbType.LongBlob).Value = p.Image;
                cmd.ExecuteNonQuery();

          
                int l = _allProdutcs.IndexOf(initial);
                _allProdutcs.RemoveAt(l);
                _allProdutcs.Insert(l, p);


            }
            catch (Exception)
            {
                MessageBox.Show("Failed to add to Product", "Error");
            }

            finally
            {
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    connection.Close();
                }
            }
            for (int i = 0; i < children.Count(); i++)
            {
                p.Cost += children.ElementAt(i).Cost * children.ElementAt(i).Quantity;


            }
            
            for (int i = 0; i < children.Count(); i++)
            {
                try
                {

                    connection.Open();
                    MySqlCommand cmd;
                    cmd = connection.CreateCommand();
                    cmd.CommandText = "Insert into tec_combo_items(product_id,item_code,quantity) Values ('" + p.Id + "','" + children.ElementAt(i).Code + "','" + children.ElementAt(i).Quantity + "')";
                    cmd.ExecuteNonQuery();



                }
                catch (Exception)
                {
                    MessageBox.Show("Failed to add to Combo Product", "Error");
                    break;
                }

                finally
                {
                    if (connection.State == System.Data.ConnectionState.Open)
                    {

                        connection.Close();
                    }
                }
            }

        }
        public static void getComboProducts(int i, ObservableCollection<Product> toreturn)
        {
            
            MySqlConnection connection = new MySqlConnection(RealUtility.dbString);

            try
            {
                connection.Open();
                string sql = "SELECT tec_products.id,tec_products.code,tec_products.name,tec_products.type,tec_products.price,tec_products.cost,tec_products.details,tec_products.alert_quantity,tec_combo_items.quantity FROM tec_products INNER JOIN tec_combo_items on tec_products.code=tec_combo_items.item_code WHERE tec_combo_items.product_id=" + i;
                using (var command = new MySqlCommand(sql, connection))
                {
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var product = new Product();
                            product.Id = int.Parse(reader["id"].ToString());
                            product.Code = reader["code"].ToString();
                           product.Name = reader["name"].ToString()+"("+ reader["code"].ToString()+")" ;
                            product.Type = reader["type"].ToString();
                            product.Price = float.Parse(reader["price"].ToString());
                            product.Cost = float.Parse(reader["cost"].ToString());
                           product.Details = reader["details"].ToString();
                           product.AlertQuantity = float.Parse(reader["alert_quantity"].ToString());
                          product.Quantity = float.Parse(reader["quantity"].ToString());




                            toreturn.Insert(0,product);
                        }

                    }
                }

            }
            catch (Exception)
            {
                MessageBox.Show("Failed to Load Combo from database", "Error");
            }

            finally
            {
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    connection.Close();
                    
                }
                
            }
            
        }

        public static void getFromDatabase()
        {

            MySqlConnection connection = new MySqlConnection(RealUtility.dbString);
            
            try
            {
                connection.Open();
                string sql = "SELECT *,tec_categories.name as x FROM tec_products INNER JOIN tec_categories on tec_products.category_id=tec_categories.id";
                using (var command = new MySqlCommand(sql, connection))
                {
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var product = new Product();
                            product.Id = int.Parse(reader["id"].ToString());
                            product.Code = reader["code"].ToString();
                            product.Name = reader["name"].ToString();

                            if (!reader["image"].ToString().Equals(""))
                            {
                               product.Image = (byte[])reader["image"];
                                product.Imagei = (ImageSource)new ImageSourceConverter().ConvertFrom(product.Image);
                                product.Vis = "Hidden";
                            }
                            product.Type = reader["type"].ToString();
                            product.Price = float.Parse(reader["price"].ToString());
                            product.Cost = float.Parse(reader["cost"].ToString());
                            product.Details = reader["details"].ToString();
                            product.CatagoryId = int.Parse(reader["category_id"].ToString());
                            product.Catagory = reader["x"].ToString();
                            product.AlertQuantity = float.Parse(reader["alert_quantity"].ToString());
                            product.Quantity = float.Parse(reader["quantity"].ToString());
                           



                            _allProdutcs.Insert(0,product);
                        }
                        
                    }
                }

            }
            catch (Exception)
            {
                MessageBox.Show("Failed to Load from database", "Error-Products"); 
            }

            finally
            {
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    connection.Close();
                }
            }
        }

    }
}
