﻿using basicPos2.models;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Microsoft.Win32;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Security.Cryptography;

namespace basicPos2
{
    static class RealUtility
    {
        public static string dbString = "SERVER=localhost;PORT=3306;DATABASE=sysonepos;UID=root;PASSWORD=1234;";
        public static string companyName = "Top Way Mobile";
        public static string address1 = "No.27 Main Street,";
        public static string address2 = "Mawanella";
        public static int userLevel = 0;
        public static int isRegisterOn = 0;
        public static int currentUserId = 1;
        public static string todaydate = Convert.ToDateTime(DateTime.Today).ToString("dd-MM-yyyy");
        
        public static string pathfornarnia = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\pos\\";

        public static Microsoft.Office.Interop.Excel.Application excel;
        public static Microsoft.Office.Interop.Excel.Workbook workBook;
        public static Microsoft.Office.Interop.Excel.Worksheet workSheet;
        public static Microsoft.Office.Interop.Excel.Range cellRange;
        public static System.Data.DataTable ToDataTable<T>(this IList<T> data)
        {
            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(T));
            System.Data.DataTable dt = new System.Data.DataTable();
            foreach (PropertyDescriptor prop in properties)
            {
                dt.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            }
            foreach (T item in data)
            {
                DataRow row = dt.NewRow();
                foreach (PropertyDescriptor pdt in properties)
                {
                    row[pdt.Name] = pdt.GetValue(item) ?? DBNull.Value;
                }
                dt.Rows.Add(row);
            }
            return dt;
        }
        public static string convto24(string s)
        {
            if (s.Count() == 8)
            {
                
                if (((s[6] + s[7]).ToString()).Equals("142"))
                {
                  
                    if (int.Parse((s[0] + s[1]).ToString()) == 99)
                    {
                        string temp =   "00" + s[2] + s[3] + s[4] ;
                        return temp;
                    }
                    else
                    {
                        return s[0]+s[1]+s[2]+s[3]+s[4]+"";
                    }
                }
                else
                {
                    MessageBox.Show(int.Parse((s[0] + s[1]).ToString())+"");
                    if (int.Parse((s[0] + s[1]).ToString()) == 99)
                    {
                        return s[0] + s[1] + s[2] + s[3] + s[4] + "";
                    }
                    else
                    {
                        string temp = (int.Parse((s[0] + s[1]).ToString()) + 12) + "" + s[2] + s[3] + s[4] ;
                        return temp;
                    }
                }
            }else
            {
                
                if (((s[5] + s[6]).ToString()).Equals("142"))
                {
                    return s[0] + s[1] + s[2] + s[3] +  "";
                }
                else
                {
                    
                    
                        string temp = (int.Parse((s[0]).ToString()) + 12) + "" + s[1] + s[2] + s[3] ;
                        return temp;
                   
                }
            }
        }
        public static void ImportFromExcel()
        {
            OpenFileDialog openfile = new OpenFileDialog();
            openfile.DefaultExt = ".xlsx";
            openfile.Filter = "(.xlsx)|*.xlsx";
            //openfile.ShowDialog();

            var browsefile = openfile.ShowDialog();

            if (browsefile == true)
            {
                String txtFilePath = openfile.FileName;

                Microsoft.Office.Interop.Excel.Application excelApp = new Microsoft.Office.Interop.Excel.Application();
                //Static File From Base Path...........
                //Microsoft.Office.Interop.Excel.Workbook excelBook = excelApp.Workbooks.Open(AppDomain.CurrentDomain.BaseDirectory + "TestExcel.xlsx", 0, true, 5, "", "", true, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
                //Dynamic File Using Uploader...........
                Microsoft.Office.Interop.Excel.Workbook excelBook = excelApp.Workbooks.Open(txtFilePath, 0, true, 5, "", "", true, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
                Microsoft.Office.Interop.Excel.Worksheet excelSheet = (Microsoft.Office.Interop.Excel.Worksheet)excelBook.Worksheets.get_Item(1); ;
                Microsoft.Office.Interop.Excel.Range excelRange = excelSheet.UsedRange;

                string strCellData = "";
                double douCellData;
                int rowCnt = 0;
                int colCnt = 0;

                System.Data.DataTable dt = new System.Data.DataTable();
                for (colCnt = 1; colCnt <= excelRange.Columns.Count; colCnt++)
                {
                    string strColumn = "";
                    strColumn = (string)(excelRange.Cells[1, colCnt] as Microsoft.Office.Interop.Excel.Range).Value2;
                    dt.Columns.Add(strColumn, typeof(string));
                }

                for (rowCnt = 2; rowCnt <= excelRange.Rows.Count; rowCnt++)
                {
                    string strData = "";
                    for (colCnt = 1; colCnt <= excelRange.Columns.Count; colCnt++)
                    {
                        try
                        {
                            strCellData = (string)(excelRange.Cells[rowCnt, colCnt] as Microsoft.Office.Interop.Excel.Range).Value2;
                            strData += strCellData + "|";
                        }
                        catch (Exception ex)
                        {
                            douCellData = (excelRange.Cells[rowCnt, colCnt] as Microsoft.Office.Interop.Excel.Range).Value2;
                            strData += douCellData.ToString() + "|";
                        }
                    }
                    strData = strData.Remove(strData.Length - 1, 1);
                    dt.Rows.Add(strData.Split('|'));
                }
                foreach (DataRow row in dt.Rows)
                {
                    Product obj = new Product()
                    {
                        Id = Product.AllProducts.Count() + 1,
                        Code = (String)row["Code"],
                        Name = (string)row["name"],
                        Type = (String)row["Type"],
                        Catagory = (string)row["Catagory"],
                        Quantity = float.Parse((String)row["Quantity"]),
                        Cost = float.Parse((String)row["Cost"]),
                        Price = float.Parse((String)row["Price"]),
                        Details = (string)row["Details"]


                    };
                    Product.AddToAllProduts(obj);
                }


                excelBook.Close(true, null, null);
                excelApp.Quit();
            }
        }
        public static void ImportFromExcelcategories()
        {
            OpenFileDialog openfile = new OpenFileDialog();
            openfile.DefaultExt = ".xlsx";
            openfile.Filter = "(.xlsx)|*.xlsx";
            //openfile.ShowDialog();

            var browsefile = openfile.ShowDialog();

            if (browsefile == true)
            {
                String txtFilePath = openfile.FileName;

                Microsoft.Office.Interop.Excel.Application excelApp = new Microsoft.Office.Interop.Excel.Application();
                //Static File From Base Path...........
                //Microsoft.Office.Interop.Excel.Workbook excelBook = excelApp.Workbooks.Open(AppDomain.CurrentDomain.BaseDirectory + "TestExcel.xlsx", 0, true, 5, "", "", true, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
                //Dynamic File Using Uploader...........
                Microsoft.Office.Interop.Excel.Workbook excelBook = excelApp.Workbooks.Open(txtFilePath, 0, true, 5, "", "", true, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
                Microsoft.Office.Interop.Excel.Worksheet excelSheet = (Microsoft.Office.Interop.Excel.Worksheet)excelBook.Worksheets.get_Item(1); ;
                Microsoft.Office.Interop.Excel.Range excelRange = excelSheet.UsedRange;

                string strCellData = "";
                double douCellData;
                int rowCnt = 0;
                int colCnt = 0;

                System.Data.DataTable dt = new System.Data.DataTable();
                for (colCnt = 1; colCnt <= excelRange.Columns.Count; colCnt++)
                {
                    string strColumn = "";
                    strColumn = (string)(excelRange.Cells[1, colCnt] as Microsoft.Office.Interop.Excel.Range).Value2;
                    dt.Columns.Add(strColumn, typeof(string));
                }

                for (rowCnt = 2; rowCnt <= excelRange.Rows.Count; rowCnt++)
                {
                    string strData = "";
                    for (colCnt = 1; colCnt <= excelRange.Columns.Count; colCnt++)
                    {
                        try
                        {
                            strCellData = (string)(excelRange.Cells[rowCnt, colCnt] as Microsoft.Office.Interop.Excel.Range).Value2;
                            strData += strCellData + "|";
                        }
                        catch (Exception ex)
                        {
                            douCellData = (excelRange.Cells[rowCnt, colCnt] as Microsoft.Office.Interop.Excel.Range).Value2;
                            strData += douCellData.ToString() + "|";
                        }
                    }
                    strData = strData.Remove(strData.Length - 1, 1);
                    dt.Rows.Add(strData.Split('|'));
                }
                foreach (DataRow row in dt.Rows)
                {
                    Catagory obj = new Catagory()
                    {
                        Id = Catagory.AllCatagories.Count() + 1,
                        Code = (String)row["Code"],
                        Name = (string)row["name"],


                    };
                    Catagory.AddToAllCatagories(obj);
                }


                excelBook.Close(true, null, null);
                excelApp.Quit();
            }
        }
        public static void Exporttocsv(DataGrid datagrid)
        {
            try { 
            datagrid.SelectAllCells();

            datagrid.ClipboardCopyMode = DataGridClipboardCopyMode.IncludeHeader;
            ApplicationCommands.Copy.Execute(null, datagrid);

            datagrid.UnselectAllCells();
            string result = (string)System.Windows.Clipboard.GetData(System.Windows.DataFormats.CommaSeparatedValue);
            System.IO.Directory.CreateDirectory(System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\pos");
            File.WriteAllText(pathfornarnia+"ProductList--"+ todaydate+".csv", "", UnicodeEncoding.UTF8);

            File.AppendAllText(RealUtility.pathfornarnia+ "ProductList--" + todaydate + ".csv", result, UnicodeEncoding.UTF8);

            MessageBox.Show("Exported to csv successfully", "Success");
            }
            catch
            {
                MessageBox.Show("Please close all instances of any CSV reader and try again", "Error");
            }
        }
        public static void Exporttocsvcategories(DataGrid datagrid)
        {try { 
            datagrid.SelectAllCells();

            datagrid.ClipboardCopyMode = DataGridClipboardCopyMode.IncludeHeader;
            ApplicationCommands.Copy.Execute(null, datagrid);

            datagrid.UnselectAllCells();
            string result = (string)System.Windows.Clipboard.GetData(System.Windows.DataFormats.CommaSeparatedValue);
            System.IO.Directory.CreateDirectory(System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\pos");

            File.WriteAllText(RealUtility.pathfornarnia+ "CategoriesList--" + todaydate + ".csv", "", UnicodeEncoding.UTF8);

            File.AppendAllText(RealUtility.pathfornarnia+ "CategoriesList--" + todaydate + ".csv", result, UnicodeEncoding.UTF8);

            MessageBox.Show("Exported to csv successfully", "Success");
            }
            catch
            {
                MessageBox.Show("Please close all instances of any CSV reader and try again", "Error");
            }
        }

        public static void ExportToPdf(DataGrid grid)
        {
            try
            {

                IEnumerable itemsSource = grid.ItemsSource as IEnumerable;
                if (itemsSource != null && Product.AllProducts.Count != 0)
                {
                    PdfPTable table = new PdfPTable(grid.Columns.Count - 2);
                    Document doc = new Document(iTextSharp.text.PageSize.A4, 25, 25, 30, 30);
                    System.IO.Directory.CreateDirectory(System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\pos");

                    PdfWriter writer = PdfWriter.GetInstance(doc, new System.IO.FileStream(RealUtility.pathfornarnia + "ProductList--" + todaydate + ".pdf", System.IO.FileMode.Create, FileAccess.ReadWrite));
                    doc.Open();
                    for (int j = 1; j < grid.Columns.Count - 1; j++)
                    {
                        table.AddCell(new Phrase(grid.Columns[j].Header.ToString()));
                    }
                    table.HeaderRows = 1;
                    foreach (var item in itemsSource)
                    {

                        DataGridRow row = grid.ItemContainerGenerator.ContainerFromItem(item) as DataGridRow;
                        if (row != null)
                        {
                            DataGridCellsPresenter presenter = FindVisualChild<DataGridCellsPresenter>(row);
                            for (int i = 1; i < grid.Columns.Count - 1; ++i)
                            {
                                DataGridCell cell = (DataGridCell)presenter.ItemContainerGenerator.ContainerFromIndex(i);
                                TextBlock txt = cell.Content as TextBlock;
                                if (txt != null)
                                {

                                    table.AddCell(new Phrase(txt.Text));
                                }
                                else
                                {
                                    table.AddCell(new Phrase(""));
                                }
                            }
                        }
                    }

                    doc.Add(table);
                    doc.Close();
                    MessageBox.Show("Successfully exported to pdf", "Success");

                }
                else
                {
                    MessageBox.Show("Cannot export empty form to pdf", "Error");
                }
            }
            catch
            {
                MessageBox.Show("Please close all instances of any PDF reader and try again","Error");
            }
        }
        public static void ExportToPdfcategories(DataGrid grid)
        {
            try { 
            System.IO.Directory.CreateDirectory(System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\pos");

            IEnumerable itemsSource = grid.ItemsSource as IEnumerable;
            if (itemsSource != null && Catagory.AllCatagories.Count != 0)
            {
                PdfPTable table = new PdfPTable(grid.Columns.Count - 2);
                Document doc = new Document(iTextSharp.text.PageSize.A4, 25, 25, 30, 30);
                PdfWriter writer = PdfWriter.GetInstance(doc, new System.IO.FileStream(pathfornarnia+ "CategoriesList--" + todaydate + ".pdf", System.IO.FileMode.Create, FileAccess.ReadWrite));
                doc.Open();
                for (int j = 1; j < grid.Columns.Count - 1; j++)
                {
                    table.AddCell(new Phrase(grid.Columns[j].Header.ToString()));
                }
                table.HeaderRows = 1;
                foreach (var item in itemsSource)
                {

                    DataGridRow row = grid.ItemContainerGenerator.ContainerFromItem(item) as DataGridRow;
                    if (row != null)
                    {
                        DataGridCellsPresenter presenter = FindVisualChild<DataGridCellsPresenter>(row);
                        for (int i = 1; i < grid.Columns.Count - 1; ++i)
                        {
                            DataGridCell cell = (DataGridCell)presenter.ItemContainerGenerator.ContainerFromIndex(i);
                            TextBlock txt = cell.Content as TextBlock;
                            if (txt != null)
                            {

                                table.AddCell(new Phrase(txt.Text));
                            }
                            else
                            {
                                table.AddCell(new Phrase(""));
                            }
                        }
                    }
                }

                doc.Add(table);
                doc.Close();
                MessageBox.Show("Successfully exported to pdf", "Success");

            }
            else
            {
                MessageBox.Show("Cannot export empty form to pdf", "Error");
            }
            }
            catch
            {
                MessageBox.Show("Please close all instances of any PDF reader and try again", "Error");
            }
        }
        public static childItem FindVisualChild<childItem>(DependencyObject obj)
                    where childItem : DependencyObject
        {
            foreach (childItem child in FindVisualChildren<childItem>(obj))
            {
                return child;
            }

            return null;
        }
        public static IEnumerable<T> FindVisualChildren<T>(DependencyObject depObj)
       where T : DependencyObject
        {
            if (depObj != null)
            {
                for (int i = 0; i < VisualTreeHelper.GetChildrenCount(depObj); i++)
                {
                    DependencyObject child = VisualTreeHelper.GetChild(depObj, i);
                    if (child != null && child is T)
                    {
                        yield return (T)child;
                    }

                    foreach (T childOfChild in FindVisualChildren<T>(child))
                    {
                        yield return childOfChild;
                    }
                }
            }
        }
        public static void GenerateExcel(System.Data.DataTable DtIN)
        {
            System.IO.Directory.CreateDirectory(System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\pos");

            try
            {
                excel = new Microsoft.Office.Interop.Excel.Application();
                excel.DisplayAlerts = false;
                excel.Visible = false;
                workBook = excel.Workbooks.Add(System.Type.Missing);
                workSheet = (Microsoft.Office.Interop.Excel.Worksheet)workBook.ActiveSheet;
                workSheet.Name = "Products";
                System.Data.DataTable tempDt = DtIN;

                workSheet.Cells.Font.Size = 11;
                int rowcount = 1;
                for (int i = 1; i <= tempDt.Columns.Count-5; i++) //taking care of Headers.  
                {
                    workSheet.Cells[1, i] = tempDt.Columns[i +4].ColumnName;
                }
                foreach (System.Data.DataRow row in tempDt.Rows) //taking care of each Row  
                {
                    rowcount += 1;
                    for (int i = 0; i < tempDt.Columns.Count-5; i++) //taking care of each column  
                    {
                        
                            workSheet.Cells[rowcount, i + 1] = row[i+5].ToString();
                        
                    }
                }
                cellRange = workSheet.Range[workSheet.Cells[1, 1], workSheet.Cells[rowcount, tempDt.Columns.Count]];
                cellRange.EntireColumn.AutoFit();

            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
                MessageBox.Show("Failed to Export", "Error");
            }
            
        }
        public static void GenerateExcelcategories(System.Data.DataTable DtIN)
        {
            try
            {
                excel = new Microsoft.Office.Interop.Excel.Application();
                excel.DisplayAlerts = false;
                excel.Visible = false;
                workBook = excel.Workbooks.Add(System.Type.Missing);
                workSheet = (Microsoft.Office.Interop.Excel.Worksheet)workBook.ActiveSheet;
                workSheet.Name = "Categories";
                System.Data.DataTable tempDt = DtIN;

                workSheet.Cells.Font.Size = 11;
                int rowcount = 1;
                for (int i = 1; i <= tempDt.Columns.Count-3; i++) //taking care of Headers.  
                {
                    workSheet.Cells[1, i] = tempDt.Columns[i +2].ColumnName;
                }
                foreach (System.Data.DataRow row in tempDt.Rows) //taking care of each Row  
                {
                    rowcount += 1;
                    for (int i = 0; i < tempDt.Columns.Count-3; i++) //taking care of each column  
                    {
                       
                            workSheet.Cells[rowcount, i + 1] = row[i+3].ToString();
                     
                    }
                }
                cellRange = workSheet.Range[workSheet.Cells[1, 1], workSheet.Cells[rowcount, tempDt.Columns.Count]];
                cellRange.EntireColumn.AutoFit();

            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
                MessageBox.Show("Failed to Export", "Error");
            }
        }
        public static void Exporttocsvsales(DataGrid datagrid)
        {
            try { 
            datagrid.SelectAllCells();
            System.IO.Directory.CreateDirectory(System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\pos");

            datagrid.ClipboardCopyMode = DataGridClipboardCopyMode.IncludeHeader;
            ApplicationCommands.Copy.Execute(null, datagrid);

            datagrid.UnselectAllCells();
            string result = (string)System.Windows.Clipboard.GetData(System.Windows.DataFormats.CommaSeparatedValue);
            File.WriteAllText(pathfornarnia+ "SalesList--" + todaydate + ".csv", "", UnicodeEncoding.UTF8);

            File.AppendAllText(pathfornarnia+ "SalesList--" + todaydate + ".csv", result, UnicodeEncoding.UTF8);

            MessageBox.Show("Exported to csv successfully", "Success");
            }
            catch
            {
                MessageBox.Show("Please close all instances of any CSV reader and try again", "Error");
            }
        }
        public static void GenerateExcelSales(System.Data.DataTable DtIN)
        {
            try
            {
                excel = new Microsoft.Office.Interop.Excel.Application();
                excel.DisplayAlerts = false;
                excel.Visible = false;
                workBook = excel.Workbooks.Add(System.Type.Missing);
                workSheet = (Microsoft.Office.Interop.Excel.Worksheet)workBook.ActiveSheet;
                workSheet.Name = "Sales";
                System.Data.DataTable tempDt = DtIN;

                workSheet.Cells.Font.Size = 11;
                int rowcount = 1;
                for (int i = 1; i <= tempDt.Columns.Count-10; i++) //taking care of Headers.  
                {
                    workSheet.Cells[1, i] = tempDt.Columns[i + 9].ColumnName;
                }
                foreach (System.Data.DataRow row in tempDt.Rows) //taking care of each Row  
                {
                    rowcount += 1;
                    for (int i = 0; i < tempDt.Columns.Count-10; i++) //taking care of each column  
                    {
                       
                            workSheet.Cells[rowcount, i + 1] = row[i+10].ToString();
                        
                    }
                }
                cellRange = workSheet.Range[workSheet.Cells[1, 1], workSheet.Cells[rowcount, tempDt.Columns.Count]];
                cellRange.EntireColumn.AutoFit();
            }
            catch (Exception)
            {
                MessageBox.Show("Failed to Export", "Error");
            }
        }
        public static void ExportToPdfSales(DataGrid grid)
        {try { 

            System.IO.Directory.CreateDirectory(System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\pos");

            IEnumerable itemsSource = grid.ItemsSource as IEnumerable;
            if (itemsSource != null && Product.AllProducts.Count != 0)
            {
                PdfPTable table = new PdfPTable(grid.Columns.Count-1);
                Document doc = new Document(iTextSharp.text.PageSize.A4, 25, 25, 30, 30);
                PdfWriter writer = PdfWriter.GetInstance(doc, new System.IO.FileStream(pathfornarnia+ "SalesList--" + todaydate + ".pdf", System.IO.FileMode.Create, FileAccess.ReadWrite));
                doc.Open();
                for (int j = 0; j < grid.Columns.Count - 1; j++)
                {
                    table.AddCell(new Phrase(grid.Columns[j].Header.ToString()));
                }
                table.HeaderRows = 1;
                foreach (var item in itemsSource)
                {

                    DataGridRow row = grid.ItemContainerGenerator.ContainerFromItem(item) as DataGridRow;
                    if (row != null)
                    {
                        DataGridCellsPresenter presenter = FindVisualChild<DataGridCellsPresenter>(row);
                        for (int i = 0; i < grid.Columns.Count-1 ; ++i)
                        {
                            DataGridCell cell = (DataGridCell)presenter.ItemContainerGenerator.ContainerFromIndex(i);
                            TextBlock txt = cell.Content as TextBlock;
                            if (txt != null)
                            {

                                table.AddCell(new Phrase(txt.Text));
                            }
                            else
                            {
                                table.AddCell(new Phrase(""));
                            }
                        }
                    }
                }

                doc.Add(table);
                doc.Close();
                MessageBox.Show("Successfully exported to pdf", "Success");

            }
            else
            {
                MessageBox.Show("Cannot export empty form to pdf", "Error");
            }
            }
            catch
            {
                MessageBox.Show("Please close all instances of any PDF reader and try again", "Error");
            }
        }
    
    public static void Exporttocsvpurchases(DataGrid datagrid)
    {
            try { 
        datagrid.SelectAllCells();
System.IO.Directory.CreateDirectory(System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\pos");
           
        datagrid.ClipboardCopyMode = DataGridClipboardCopyMode.IncludeHeader;
        ApplicationCommands.Copy.Execute(null, datagrid);

        datagrid.UnselectAllCells();
        string result = (string)System.Windows.Clipboard.GetData(System.Windows.DataFormats.CommaSeparatedValue);
        File.WriteAllText(pathfornarnia+ "PurchasesList--" + todaydate + ".csv", "", UnicodeEncoding.UTF8);

        File.AppendAllText(pathfornarnia+ "PurchasesList--" + todaydate + ".csv", result, UnicodeEncoding.UTF8);

        MessageBox.Show("Exported to csv successfully", "Success");
            }
            catch
            {
                MessageBox.Show("Please close all instances of any CSV reader and try again", "Error");
            }
        }
    public static void GenerateExcelPurchases(System.Data.DataTable DtIN)
    {
        try
        {
            excel = new Microsoft.Office.Interop.Excel.Application();
            excel.DisplayAlerts = false;
            excel.Visible = false;
            workBook = excel.Workbooks.Add(System.Type.Missing);
            workSheet = (Microsoft.Office.Interop.Excel.Worksheet)workBook.ActiveSheet;
            workSheet.Name = "Purchases";
            System.Data.DataTable tempDt = DtIN;

            workSheet.Cells.Font.Size = 11;
            int rowcount = 1;
            for (int i = 1; i <= tempDt.Columns.Count-5; i++) //taking care of Headers.  
            {
                    if (i != 1)
                    {
                        workSheet.Cells[1, i] = tempDt.Columns[i + 4].ColumnName;
                    }
                    else
                    {
                        workSheet.Cells[1, i] = "Date";

                    }
                }
            foreach (System.Data.DataRow row in tempDt.Rows) //taking care of each Row  
            {
                rowcount += 1;
                for (int i = 0; i < tempDt.Columns.Count-5; i++) //taking care of each column  
                {
                   
                    
                        workSheet.Cells[rowcount, i + 1] = row[i+5].ToString();
                    
                }
            }
            cellRange = workSheet.Range[workSheet.Cells[1, 1], workSheet.Cells[rowcount, tempDt.Columns.Count]];
            cellRange.EntireColumn.AutoFit();
        }
        catch (Exception)
        {
            MessageBox.Show("Failed to Export", "Error");
        }
    }
    public static void ExportToPdfPurchases(DataGrid grid)
    {
            try { 
            System.IO.Directory.CreateDirectory(System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\pos");

            IEnumerable itemsSource = grid.ItemsSource as IEnumerable;
        if (itemsSource != null && basicPos2.models.Purchases.AllPurchases.Count != 0)
        {
            PdfPTable table = new PdfPTable(grid.Columns.Count - 1);
            Document doc = new Document(iTextSharp.text.PageSize.A4, 25, 25, 30, 30);
            PdfWriter writer = PdfWriter.GetInstance(doc, new System.IO.FileStream(pathfornarnia+ "PurchasesList--" + todaydate + ".pdf", System.IO.FileMode.Create, FileAccess.ReadWrite));
            doc.Open();
            for (int j = 0; j < grid.Columns.Count - 1; j++)
            {
                table.AddCell(new Phrase(grid.Columns[j].Header.ToString()));
            }
            table.HeaderRows = 1;
            foreach (var item in itemsSource)
            {

                DataGridRow row = grid.ItemContainerGenerator.ContainerFromItem(item) as DataGridRow;
                if (row != null)
                {
                    DataGridCellsPresenter presenter = FindVisualChild<DataGridCellsPresenter>(row);
                    for (int i = 0; i < grid.Columns.Count-1; ++i)
                    {
                        DataGridCell cell = (DataGridCell)presenter.ItemContainerGenerator.ContainerFromIndex(i);
                        TextBlock txt = cell.Content as TextBlock;
                        if (txt != null)
                        {

                            table.AddCell(new Phrase(txt.Text));
                        }
                        else
                        {
                            table.AddCell(new Phrase(""));
                        }
                    }
                }
            }

            doc.Add(table);
            doc.Close();
            MessageBox.Show("Successfully exported to pdf", "Success");

        }
        else
        {
            MessageBox.Show("Cannot export empty form to pdf", "Error");
        }
            }
            catch
            {
                MessageBox.Show("Please close all instances of any PDF reader and try again", "Error");
            }
        }
        public static void Exporttocsvexpenses(DataGrid datagrid)
        {
            try { 
            datagrid.SelectAllCells();
            datagrid.ClipboardCopyMode = DataGridClipboardCopyMode.IncludeHeader;
            ApplicationCommands.Copy.Execute(null, datagrid);

            datagrid.UnselectAllCells();
            string result = (string)System.Windows.Clipboard.GetData(System.Windows.DataFormats.CommaSeparatedValue);
            File.WriteAllText(pathfornarnia+ "ExpensesList--" + todaydate + ".csv", "", UnicodeEncoding.UTF8);

            File.AppendAllText(pathfornarnia+ "ExpensesList--" + todaydate + ".csv", result, UnicodeEncoding.UTF8);

            MessageBox.Show("Exported to csv successfully", "Success");
            }
            catch
            {
                MessageBox.Show("Please close all instances of any CSV reader and try again", "Error");
            }
        }
        public static void GenerateExcelExpenses(System.Data.DataTable DtIN)
        {
            try
            {
                excel = new Microsoft.Office.Interop.Excel.Application();
                excel.DisplayAlerts = false;
                excel.Visible = false;
                workBook = excel.Workbooks.Add(System.Type.Missing);
                workSheet = (Microsoft.Office.Interop.Excel.Worksheet)workBook.ActiveSheet;
                workSheet.Name = "Expenses";
                System.Data.DataTable tempDt = DtIN;

                workSheet.Cells.Font.Size = 11;
                int rowcount = 1;
                for (int i = 1; i <= tempDt.Columns.Count-3; i++) //taking care of Headers.  
                {
                    if (i != 1)
                    {
                        workSheet.Cells[1, i] = tempDt.Columns[i + 2].ColumnName;
                    }
                    else
                    {
                        workSheet.Cells[1, i] = "Date";

                    }
                }
                foreach (System.Data.DataRow row in tempDt.Rows) //taking care of each Row  
                {
                    rowcount += 1;
                    for (int i = 0; i < tempDt.Columns.Count-3; i++) //taking care of each column  
                    {
                       
                            workSheet.Cells[rowcount, i + 1] = row[i+3].ToString();
                        
                    }
                }
                cellRange = workSheet.Range[workSheet.Cells[1, 1], workSheet.Cells[rowcount, tempDt.Columns.Count]];
                cellRange.EntireColumn.AutoFit();
            }
            catch (Exception)
            {
                MessageBox.Show("Failed to Export", "Error");
            }
        }
        public static void ExportToPdfExpenses(DataGrid grid)
        {
            try { 
            System.IO.Directory.CreateDirectory(System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\pos");

            IEnumerable itemsSource = grid.ItemsSource as IEnumerable;
            if (itemsSource != null && basicPos2.models.Purchases.AllPurchases.Count != 0)
            {
                PdfPTable table = new PdfPTable(grid.Columns.Count - 1);
                Document doc = new Document(iTextSharp.text.PageSize.A4, 25, 25, 30, 30);
                PdfWriter writer = PdfWriter.GetInstance(doc, new System.IO.FileStream(pathfornarnia+ "ExpensesList--" + todaydate + ".pdf", System.IO.FileMode.Create, FileAccess.ReadWrite));
                doc.Open();
                for (int j = 0; j < grid.Columns.Count - 1; j++)
                {
                    table.AddCell(new Phrase(grid.Columns[j].Header.ToString()));
                }
                table.HeaderRows = 1;
                foreach (var item in itemsSource)
                {

                    DataGridRow row = grid.ItemContainerGenerator.ContainerFromItem(item) as DataGridRow;
                    if (row != null)
                    {
                        DataGridCellsPresenter presenter = FindVisualChild<DataGridCellsPresenter>(row);
                        for (int i = 0; i < grid.Columns.Count - 1; ++i)
                        {
                            DataGridCell cell = (DataGridCell)presenter.ItemContainerGenerator.ContainerFromIndex(i);
                            TextBlock txt = cell.Content as TextBlock;
                            if (txt != null)
                            {

                                table.AddCell(new Phrase(txt.Text));
                            }
                            else
                            {
                                table.AddCell(new Phrase(""));
                            }
                        }
                    }
                }

                doc.Add(table);
                doc.Close();
                MessageBox.Show("Successfully exported to pdf", "Success");

            }
            else
            {
                MessageBox.Show("Cannot export empty form to pdf", "Error");
            }
            }
            catch
            {
                MessageBox.Show("Please close all instances of any PDF reader and try again", "Error");
            }
        }
        public static void Exporttocsvsupliers(DataGrid datagrid)
        {
            try { 
            datagrid.SelectAllCells();
            System.IO.Directory.CreateDirectory(System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\pos");

            datagrid.ClipboardCopyMode = DataGridClipboardCopyMode.IncludeHeader;
            ApplicationCommands.Copy.Execute(null, datagrid);

            datagrid.UnselectAllCells();
            string result = (string)System.Windows.Clipboard.GetData(System.Windows.DataFormats.CommaSeparatedValue);
            File.WriteAllText(pathfornarnia+ "SupliersList--" + todaydate + ".csv", "", UnicodeEncoding.UTF8);

            File.AppendAllText(pathfornarnia+ "SupliersList--" + todaydate + ".csv", result, UnicodeEncoding.UTF8);

            MessageBox.Show("Exported to csv successfully", "Success");
            }
            catch
            {
                MessageBox.Show("Please close all instances of any CSV reader and try again", "Error");
            }
        }
        public static void GenerateExcelSuppliers(System.Data.DataTable DtIN)
        {
            try
            {
                excel = new Microsoft.Office.Interop.Excel.Application();
                excel.DisplayAlerts = false;
                excel.Visible = false;
                workBook = excel.Workbooks.Add(System.Type.Missing);
                workSheet = (Microsoft.Office.Interop.Excel.Worksheet)workBook.ActiveSheet;
                workSheet.Name = "Vendors";
                System.Data.DataTable tempDt = DtIN;

                workSheet.Cells.Font.Size = 11;
                int rowcount = 1;
                for (int i = 1; i <= tempDt.Columns.Count-1; i++) //taking care of Headers.  
                {
                    workSheet.Cells[1, i] = tempDt.Columns[i ].ColumnName;
                }
                foreach (System.Data.DataRow row in tempDt.Rows) //taking care of each Row  
                {
                    rowcount += 1;
                    for (int i = 0; i < tempDt.Columns.Count-1; i++) //taking care of each column  
                    {
                       
                            workSheet.Cells[rowcount, i + 1] = row[i+1].ToString();
                        
                    }
                }
                cellRange = workSheet.Range[workSheet.Cells[1, 1], workSheet.Cells[rowcount, tempDt.Columns.Count]];
                cellRange.EntireColumn.AutoFit();
            }
            catch (Exception)
            {
                MessageBox.Show("Failed to Export", "Error");
            }
        }
        public static void ExportToPdfSuppliers(DataGrid grid)
        {
            try { 
            System.IO.Directory.CreateDirectory(System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\pos");

            IEnumerable itemsSource = grid.ItemsSource as IEnumerable;
            if (itemsSource != null && basicPos2.models.Purchases.AllPurchases.Count != 0)
            {
                PdfPTable table = new PdfPTable(grid.Columns.Count - 1);
                Document doc = new Document(iTextSharp.text.PageSize.A4, 25, 25, 30, 30);
                PdfWriter writer = PdfWriter.GetInstance(doc, new System.IO.FileStream(pathfornarnia+ "SupliersList--" + todaydate + ".pdf", System.IO.FileMode.Create, FileAccess.ReadWrite));
                doc.Open();
                for (int j = 0; j < grid.Columns.Count - 1; j++)
                {
                    table.AddCell(new Phrase(grid.Columns[j].Header.ToString()));
                }
                table.HeaderRows = 1;
                foreach (var item in itemsSource)
                {

                    DataGridRow row = grid.ItemContainerGenerator.ContainerFromItem(item) as DataGridRow;
                    if (row != null)
                    {
                        DataGridCellsPresenter presenter = FindVisualChild<DataGridCellsPresenter>(row);
                        for (int i = 0; i < grid.Columns.Count - 1; ++i)
                        {
                            DataGridCell cell = (DataGridCell)presenter.ItemContainerGenerator.ContainerFromIndex(i);
                            TextBlock txt = cell.Content as TextBlock;
                            if (txt != null)
                            {

                                table.AddCell(new Phrase(txt.Text));
                            }
                            else
                            {
                                table.AddCell(new Phrase(""));
                            }
                        }
                    }
                }

                doc.Add(table);
                doc.Close();
                MessageBox.Show("Successfully exported to pdf", "Success");

            }
            else
            {
                MessageBox.Show("Cannot export empty form to pdf", "Error");
            }
            }
            catch
            {
                MessageBox.Show("Please close all instances of any PDF reader and try again", "Error");
            }
        }
        public static void Exporttocsvcustomers(DataGrid datagrid)
        {
            try { 
            datagrid.SelectAllCells();
            datagrid.ClipboardCopyMode = DataGridClipboardCopyMode.IncludeHeader;
            ApplicationCommands.Copy.Execute(null, datagrid);
            System.IO.Directory.CreateDirectory(System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\pos");

            datagrid.UnselectAllCells();
            string result = (string)System.Windows.Clipboard.GetData(System.Windows.DataFormats.CommaSeparatedValue);
            File.WriteAllText(pathfornarnia + "CustomersList--" + todaydate + ".csv", "", UnicodeEncoding.UTF8);

            File.AppendAllText(pathfornarnia+ "CustomersList--" + todaydate + ".csv", result, UnicodeEncoding.UTF8);

            MessageBox.Show("Exported to csv successfully", "Success");
            }
            catch
            {
                MessageBox.Show("Please close all instances of any CSV reader and try again", "Error");
            }
        }
        public static void GenerateExcelCustomers(System.Data.DataTable DtIN)
        {
            try
            {
                excel = new Microsoft.Office.Interop.Excel.Application();
                excel.DisplayAlerts = false;
                excel.Visible = false;
                workBook = excel.Workbooks.Add(System.Type.Missing);
                workSheet = (Microsoft.Office.Interop.Excel.Worksheet)workBook.ActiveSheet;
                workSheet.Name = "Customers";
                System.Data.DataTable tempDt = DtIN;

                workSheet.Cells.Font.Size = 11;
                int rowcount = 1;
                for (int i = 0; i <= tempDt.Columns.Count-1; i++) //taking care of Headers.  
                {
                    workSheet.Cells[1, i] = tempDt.Columns[i ].ColumnName;
                }
                foreach (System.Data.DataRow row in tempDt.Rows) //taking care of each Row  
                {
                    rowcount += 1;
                    for (int i = 0; i < tempDt.Columns.Count-1; i++) //taking care of each column  
                    {
                       
                            workSheet.Cells[rowcount, i + 1] = row[i+1].ToString();
                        
                    }
                }
                cellRange = workSheet.Range[workSheet.Cells[1, 1], workSheet.Cells[rowcount, tempDt.Columns.Count]];
                cellRange.EntireColumn.AutoFit();
            }
            catch (Exception)
            {
                MessageBox.Show("Failed to Export", "Error");
            }
        }
        public static void ExportToPdfCustomers(DataGrid grid)
        {
            try { 
            System.IO.Directory.CreateDirectory(System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\pos");

            IEnumerable itemsSource = grid.ItemsSource as IEnumerable;
            if (itemsSource != null && basicPos2.models.Purchases.AllPurchases.Count != 0)
            {
                PdfPTable table = new PdfPTable(grid.Columns.Count - 1);
                Document doc = new Document(iTextSharp.text.PageSize.A4, 25, 25, 30, 30);
                PdfWriter writer = PdfWriter.GetInstance(doc, new System.IO.FileStream(pathfornarnia + "CustomersList--" + todaydate + ".pdf", System.IO.FileMode.Create, FileAccess.ReadWrite));
                doc.Open();
                for (int j = 0; j < grid.Columns.Count - 1; j++)
                {
                    table.AddCell(new Phrase(grid.Columns[j].Header.ToString()));
                }
                table.HeaderRows = 1;
                foreach (var item in itemsSource)
                {

                    DataGridRow row = grid.ItemContainerGenerator.ContainerFromItem(item) as DataGridRow;
                    if (row != null)
                    {
                        DataGridCellsPresenter presenter = FindVisualChild<DataGridCellsPresenter>(row);
                        for (int i = 0; i < grid.Columns.Count - 1; ++i)
                        {
                            DataGridCell cell = (DataGridCell)presenter.ItemContainerGenerator.ContainerFromIndex(i);
                            TextBlock txt = cell.Content as TextBlock;
                            if (txt != null)
                            {

                                table.AddCell(new Phrase(txt.Text));
                            }
                            else
                            {
                                table.AddCell(new Phrase(""));
                            }
                        }
                    }
                }

                doc.Add(table);
                doc.Close();
                MessageBox.Show("Successfully exported to pdf", "Success");

            }
            else
            {
                MessageBox.Show("Cannot export empty form to pdf", "Error");
            }
            }
            catch
            {
                MessageBox.Show("Please close all instances of any PDF reader and try again", "Error");
            }
        }
        public static void Exporttocsvreport(DataGrid datagrid)
        {try { 
            datagrid.SelectAllCells();
            datagrid.ClipboardCopyMode = DataGridClipboardCopyMode.IncludeHeader;
            ApplicationCommands.Copy.Execute(null, datagrid);
            System.IO.Directory.CreateDirectory(System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\pos");

            datagrid.UnselectAllCells();
            string result = (string)System.Windows.Clipboard.GetData(System.Windows.DataFormats.CommaSeparatedValue);
            File.WriteAllText(pathfornarnia + "DailySalesReport--" + todaydate + ".csv", "", UnicodeEncoding.UTF8);

            File.AppendAllText(pathfornarnia + "DailySalesReport--" + todaydate + ".csv", result, UnicodeEncoding.UTF8);

            MessageBox.Show("Exported to csv successfully", "Success");
            }
            catch
            {
                MessageBox.Show("Please close all instances of any CSV reader and try again", "Error");
            }
        }
        public static void GenerateExcelreport(System.Data.DataTable DtIN)
        {
            try
            {
                excel = new Microsoft.Office.Interop.Excel.Application();
                excel.DisplayAlerts = false;
                excel.Visible = false;
                workBook = excel.Workbooks.Add(System.Type.Missing);
                workSheet = (Microsoft.Office.Interop.Excel.Worksheet)workBook.ActiveSheet;
                workSheet.Name = "Daily Sales Report";
                System.Data.DataTable tempDt = DtIN;

                workSheet.Cells.Font.Size = 11;
                int rowcount = 1;
                for (int i = 1; i <= tempDt.Columns.Count-1; i++) //taking care of Headers.  
                {
                    workSheet.Cells[1, i] = tempDt.Columns[i ].ColumnName;
                }
                foreach (System.Data.DataRow row in tempDt.Rows) //taking care of each Row  
                {
                    rowcount += 1;
                    for (int i = 0; i < tempDt.Columns.Count-1; i++) //taking care of each column  
                    {
                        
                            workSheet.Cells[rowcount, i + 1] = row[i+1].ToString();
                        
                    }
                }
                cellRange = workSheet.Range[workSheet.Cells[1, 1], workSheet.Cells[rowcount, tempDt.Columns.Count]];
                cellRange.EntireColumn.AutoFit();
            }
            catch (Exception)
            {
                MessageBox.Show("Failed to Export", "Error");
            }
        }
        public static void ExportToPdfreport(DataGrid grid)
        {
            try { 
            System.IO.Directory.CreateDirectory(System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\pos");

            IEnumerable itemsSource = grid.ItemsSource as IEnumerable;
            if (itemsSource != null && basicPos2.models.Purchases.AllPurchases.Count != 0)
            {
                PdfPTable table = new PdfPTable(grid.Columns.Count);
                Document doc = new Document(iTextSharp.text.PageSize.A4, 25, 25, 30, 30);
                PdfWriter writer = PdfWriter.GetInstance(doc, new System.IO.FileStream(pathfornarnia + "DailySalesReport--" + todaydate + ".pdf", System.IO.FileMode.Create, FileAccess.ReadWrite));
                doc.Open();
                for (int j = 0; j < grid.Columns.Count ; j++)
                {
                    table.AddCell(new Phrase(grid.Columns[j].Header.ToString()));
                }
                table.HeaderRows = 1;
                foreach (var item in itemsSource)
                {

                    DataGridRow row = grid.ItemContainerGenerator.ContainerFromItem(item) as DataGridRow;
                    if (row != null)
                    {
                        DataGridCellsPresenter presenter = FindVisualChild<DataGridCellsPresenter>(row);
                        for (int i = 0; i < grid.Columns.Count ; ++i)
                        {
                            DataGridCell cell = (DataGridCell)presenter.ItemContainerGenerator.ContainerFromIndex(i);
                            TextBlock txt = cell.Content as TextBlock;
                            if (txt != null)
                            {

                                table.AddCell(new Phrase(txt.Text));
                            }
                            else
                            {
                                table.AddCell(new Phrase(""));
                            }
                        }
                    }
                }

                doc.Add(table);
                doc.Close();
                MessageBox.Show("Successfully exported to pdf", "Success");

            }
            else
            {
                MessageBox.Show("Cannot export empty form to pdf", "Error");
            }
            }
            catch
            {
                MessageBox.Show("Please close all instances of any PDF reader and try again", "Error");
            }
        }
        public static void Exporttocsvpurchasereport(DataGrid datagrid)
        {try { 
            datagrid.SelectAllCells();
            datagrid.ClipboardCopyMode = DataGridClipboardCopyMode.IncludeHeader;
            ApplicationCommands.Copy.Execute(null, datagrid);
            System.IO.Directory.CreateDirectory(System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\pos");

            datagrid.UnselectAllCells();
            string result = (string)System.Windows.Clipboard.GetData(System.Windows.DataFormats.CommaSeparatedValue);
            File.WriteAllText(pathfornarnia + "DailyPurchasesReport--" + todaydate + ".csv", "", UnicodeEncoding.UTF8);

            File.AppendAllText(pathfornarnia + "DailyPurchasesReport--" + todaydate + ".csv", result, UnicodeEncoding.UTF8);

            MessageBox.Show("Exported to csv successfully", "Success");
            }
            catch
            {
                MessageBox.Show("Please close all instances of any CSV reader and try again", "Error");
            }
        }
        public static void GenerateExcelpurchasereport(System.Data.DataTable DtIN)
        {
            try
            {
                excel = new Microsoft.Office.Interop.Excel.Application();
                excel.DisplayAlerts = false;
                excel.Visible = false;
                workBook = excel.Workbooks.Add(System.Type.Missing);
                workSheet = (Microsoft.Office.Interop.Excel.Worksheet)workBook.ActiveSheet;
                workSheet.Name = "Daily Purchases Report";
                System.Data.DataTable tempDt = DtIN;

                workSheet.Cells.Font.Size = 11;
                int rowcount = 1;
                for (int i = 1; i <= tempDt.Columns.Count-4; i++) //taking care of Headers.  
                {
                    workSheet.Cells[1, i] = tempDt.Columns[i ].ColumnName;
                }
                foreach (System.Data.DataRow row in tempDt.Rows) //taking care of each Row  
                {
                    rowcount += 1;
                    for (int i = 0; i < tempDt.Columns.Count-4; i++) //taking care of each column  
                    {
                        
                            workSheet.Cells[rowcount, i + 1] = row[i+1].ToString();
                        
                    }
                }
                cellRange = workSheet.Range[workSheet.Cells[1, 1], workSheet.Cells[rowcount, tempDt.Columns.Count]];
                cellRange.EntireColumn.AutoFit();
            }
            catch (Exception)
            {
                MessageBox.Show("Failed to Export", "Error");
            }
        }
        public static void ExportToPdfpurchasereport(DataGrid grid)
        {
            try { 
            System.IO.Directory.CreateDirectory(System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\pos");

            IEnumerable itemsSource = grid.ItemsSource as IEnumerable;
            if (itemsSource != null && basicPos2.models.Purchases.AllPurchases.Count != 0)
            {
                PdfPTable table = new PdfPTable(grid.Columns.Count);
                Document doc = new Document(iTextSharp.text.PageSize.A4, 25, 25, 30, 30);
                PdfWriter writer = PdfWriter.GetInstance(doc, new System.IO.FileStream(pathfornarnia + "DailyPurchasesReport--" + todaydate + ".pdf", System.IO.FileMode.Create, FileAccess.ReadWrite));
                doc.Open();
                for (int j = 0; j < grid.Columns.Count; j++)
                {
                    table.AddCell(new Phrase(grid.Columns[j].Header.ToString()));
                }
                table.HeaderRows = 1;
                foreach (var item in itemsSource)
                {

                    DataGridRow row = grid.ItemContainerGenerator.ContainerFromItem(item) as DataGridRow;
                    if (row != null)
                    {
                        DataGridCellsPresenter presenter = FindVisualChild<DataGridCellsPresenter>(row);
                        for (int i = 0; i < grid.Columns.Count; ++i)
                        {
                            DataGridCell cell = (DataGridCell)presenter.ItemContainerGenerator.ContainerFromIndex(i);
                            TextBlock txt = cell.Content as TextBlock;
                            if (txt != null)
                            {

                                table.AddCell(new Phrase(txt.Text));
                            }
                            else
                            {
                                table.AddCell(new Phrase(""));
                            }
                        }
                    }
                }

                doc.Add(table);
                doc.Close();
                MessageBox.Show("Successfully exported to pdf", "Success");

            }
            else
            {
                MessageBox.Show("Cannot export empty form to pdf", "Error");
            }
            }
            catch
            {
                MessageBox.Show("Please close all instances of any PDF reader and try again", "Error");
            }
        }
        public static void Exporttocsvexpensereport(DataGrid datagrid)
        {try { 
            datagrid.SelectAllCells();
            datagrid.ClipboardCopyMode = DataGridClipboardCopyMode.IncludeHeader;
            ApplicationCommands.Copy.Execute(null, datagrid);
            System.IO.Directory.CreateDirectory(System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\pos");

            datagrid.UnselectAllCells();
            string result = (string)System.Windows.Clipboard.GetData(System.Windows.DataFormats.CommaSeparatedValue);
            File.WriteAllText(pathfornarnia + "DailyExpensesReport--" + todaydate + ".csv", "", UnicodeEncoding.UTF8);

            File.AppendAllText(pathfornarnia + "DailyExpensesReport--" + todaydate + ".csv", result, UnicodeEncoding.UTF8);

            MessageBox.Show("Exported to csv successfully", "Success");
            }
            catch
            {
                MessageBox.Show("Please close all instances of any CSV reader and try again", "Error");
            }
        }
        public static void GenerateExcelexpensereport(System.Data.DataTable DtIN)
        {
            try
            {
                excel = new Microsoft.Office.Interop.Excel.Application();
                excel.DisplayAlerts = false;
                excel.Visible = false;
                workBook = excel.Workbooks.Add(System.Type.Missing);
                workSheet = (Microsoft.Office.Interop.Excel.Worksheet)workBook.ActiveSheet;
                workSheet.Name = "Daily Expenses Report";
                System.Data.DataTable tempDt = DtIN;

                workSheet.Cells.Font.Size = 11;
                int rowcount = 1;
                for (int i = 1; i <= tempDt.Columns.Count-4; i++) //taking care of Headers.  
                {
                    workSheet.Cells[1, i] = tempDt.Columns[i].ColumnName;
                }
                foreach (System.Data.DataRow row in tempDt.Rows) //taking care of each Row  
                {
                    rowcount += 1;
                    for (int i = 0; i < tempDt.Columns.Count-4; i++) //taking care of each column  
                    {
                       
                            workSheet.Cells[rowcount, i + 1] = row[i+1].ToString();
                        
                    }
                }
                cellRange = workSheet.Range[workSheet.Cells[1, 1], workSheet.Cells[rowcount, tempDt.Columns.Count]];
                cellRange.EntireColumn.AutoFit();
            }
            catch (Exception)
            {
                MessageBox.Show("Failed to Export", "Error");
            }
        }
        public static void ExportToPdfexpensereport(DataGrid grid)
        {
            try { 
            System.IO.Directory.CreateDirectory(System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\pos");

            IEnumerable itemsSource = grid.ItemsSource as IEnumerable;
            if (itemsSource != null && basicPos2.models.Purchases.AllPurchases.Count != 0)
            {
                PdfPTable table = new PdfPTable(grid.Columns.Count);
                Document doc = new Document(iTextSharp.text.PageSize.A4, 25, 25, 30, 30);
                PdfWriter writer = PdfWriter.GetInstance(doc, new System.IO.FileStream(pathfornarnia + "DailyExpensesReport--" + todaydate + ".pdf", System.IO.FileMode.Create, FileAccess.ReadWrite));
                doc.Open();
                for (int j = 0; j < grid.Columns.Count; j++)
                {
                    table.AddCell(new Phrase(grid.Columns[j].Header.ToString()));
                }
                table.HeaderRows = 1;
                foreach (var item in itemsSource)
                {

                    DataGridRow row = grid.ItemContainerGenerator.ContainerFromItem(item) as DataGridRow;
                    if (row != null)
                    {
                        DataGridCellsPresenter presenter = FindVisualChild<DataGridCellsPresenter>(row);
                        for (int i = 0; i < grid.Columns.Count; ++i)
                        {
                            DataGridCell cell = (DataGridCell)presenter.ItemContainerGenerator.ContainerFromIndex(i);
                            TextBlock txt = cell.Content as TextBlock;
                            if (txt != null)
                            {

                                table.AddCell(new Phrase(txt.Text));
                            }
                            else
                            {
                                table.AddCell(new Phrase(""));
                            }
                        }
                    }
                }

                doc.Add(table);
                doc.Close();
                MessageBox.Show("Successfully exported to pdf", "Success");

            }
            else
            {
                MessageBox.Show("Cannot export empty form to pdf", "Error");
            }
            }
            catch
            {
                MessageBox.Show("Please close all instances of any PDF reader and try again", "Error");
            }
        }
        public static void Exporttocsvmonthlyreport(DataGrid datagrid)
        {try { 
            datagrid.SelectAllCells();

            datagrid.ClipboardCopyMode = DataGridClipboardCopyMode.IncludeHeader;
            ApplicationCommands.Copy.Execute(null, datagrid);
            System.IO.Directory.CreateDirectory(System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\pos");

            datagrid.UnselectAllCells();
            string result = (string)System.Windows.Clipboard.GetData(System.Windows.DataFormats.CommaSeparatedValue);
            File.WriteAllText(pathfornarnia + "MonthlySalesReport--" + todaydate + ".csv", "", UnicodeEncoding.UTF8);

            File.AppendAllText(pathfornarnia + "MonthlySalesReport--" + todaydate + ".csv", result, UnicodeEncoding.UTF8);

            MessageBox.Show("Exported to csv successfully", "Success");
            }
            catch
            {
                MessageBox.Show("Please close all instances of any CSV reader and try again", "Error");
            }
        }
        public static void GenerateExcelmonthlyreport(System.Data.DataTable DtIN)
        {
            try
            {
                excel = new Microsoft.Office.Interop.Excel.Application();
                excel.DisplayAlerts = false;
                excel.Visible = false;
                workBook = excel.Workbooks.Add(System.Type.Missing);
                workSheet = (Microsoft.Office.Interop.Excel.Worksheet)workBook.ActiveSheet;
                workSheet.Name = "Monthly Sales Report";
                System.Data.DataTable tempDt = DtIN;

                workSheet.Cells.Font.Size = 11;
                int rowcount = 1;
                for (int i = 1; i <= tempDt.Columns.Count; i++) //taking care of Headers.  
                {
                    workSheet.Cells[1, i] = tempDt.Columns[i-1 ].ColumnName;
                }
                foreach (System.Data.DataRow row in tempDt.Rows) //taking care of each Row  
                {
                    rowcount += 1;
                    for (int i = 0; i < tempDt.Columns.Count; i++) //taking care of each column  
                    {
                       
                            workSheet.Cells[rowcount, i + 1] = row[i].ToString();
                        
                    }
                }
                cellRange = workSheet.Range[workSheet.Cells[1, 1], workSheet.Cells[rowcount, tempDt.Columns.Count]];
                cellRange.EntireColumn.AutoFit();
            }
            catch (Exception)
            {
                MessageBox.Show("Failed to Export", "Error");
            }
        }
        public static void ExportToPdfMonthlyreport(DataGrid grid)
        {

            try { 
            System.IO.Directory.CreateDirectory(System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\pos");


            IEnumerable itemsSource = grid.ItemsSource as IEnumerable;
            if (itemsSource != null && basicPos2.models.Purchases.AllPurchases.Count != 0)
            {
                PdfPTable table = new PdfPTable(grid.Columns.Count);
                Document doc = new Document(iTextSharp.text.PageSize.A4, 25, 25, 30, 30);
                PdfWriter writer = PdfWriter.GetInstance(doc, new System.IO.FileStream(pathfornarnia+ "MonthlySalesReport--" + todaydate + ".pdf", System.IO.FileMode.Create, FileAccess.ReadWrite));
                doc.Open();
                for (int j = 0; j < grid.Columns.Count; j++)
                {
                    table.AddCell(new Phrase(grid.Columns[j].Header.ToString()));
                }
                table.HeaderRows = 1;
                foreach (var item in itemsSource)
                {

                    DataGridRow row = grid.ItemContainerGenerator.ContainerFromItem(item) as DataGridRow;
                    if (row != null)
                    {
                        DataGridCellsPresenter presenter = FindVisualChild<DataGridCellsPresenter>(row);
                        for (int i = 0; i < grid.Columns.Count; ++i)
                        {
                            DataGridCell cell = (DataGridCell)presenter.ItemContainerGenerator.ContainerFromIndex(i);
                            TextBlock txt = cell.Content as TextBlock;
                            if (txt != null)
                            {

                                table.AddCell(new Phrase(txt.Text));
                            }
                            else
                            {
                                table.AddCell(new Phrase(""));
                            }
                        }
                    }
                }

                doc.Add(table);
                doc.Close();
                MessageBox.Show("Successfully exported to pdf", "Success");

            }
            else
            {
                MessageBox.Show("Cannot export empty form to pdf", "Error");
            }
            }
            catch
            {
                MessageBox.Show("Please close all instances of any PDF reader and try again", "Error");
            }
        }
        public static void Exporttocsvmonthlypurchasereport(DataGrid datagrid)
        {try { 
            datagrid.SelectAllCells();

            datagrid.ClipboardCopyMode = DataGridClipboardCopyMode.IncludeHeader;
            ApplicationCommands.Copy.Execute(null, datagrid);
            System.IO.Directory.CreateDirectory(System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\pos");

            datagrid.UnselectAllCells();
            string result = (string)System.Windows.Clipboard.GetData(System.Windows.DataFormats.CommaSeparatedValue);
            File.WriteAllText(pathfornarnia+ "MonthlyPurchasesReport--" + todaydate + ".csv", "", UnicodeEncoding.UTF8);

            File.AppendAllText(pathfornarnia + "MonthlyPurchasesReport--" + todaydate + ".csv", result, UnicodeEncoding.UTF8);

            MessageBox.Show("Exported to csv successfully", "Success");
            }
            catch
            {
                MessageBox.Show("Please close all instances of any CSV reader and try again", "Error");
            }
        }
        public static void GenerateExcelmonthlypurchasereport(System.Data.DataTable DtIN)
        {
            try
            {
                excel = new Microsoft.Office.Interop.Excel.Application();
                excel.DisplayAlerts = false;
                excel.Visible = false;
                workBook = excel.Workbooks.Add(System.Type.Missing);
                workSheet = (Microsoft.Office.Interop.Excel.Worksheet)workBook.ActiveSheet;
                workSheet.Name = "Monthly Purchases Report";
                System.Data.DataTable tempDt = DtIN;

                workSheet.Cells.Font.Size = 11;
                int rowcount = 1;
                for (int i = 1; i <= tempDt.Columns.Count-3; i++) //taking care of Headers.  
                {
                    workSheet.Cells[1, i] = tempDt.Columns[i -1].ColumnName;
                }
                foreach (System.Data.DataRow row in tempDt.Rows) //taking care of each Row  
                {
                    rowcount += 1;
                    for (int i = 0; i < tempDt.Columns.Count-3; i++) //taking care of each column  
                    {
                      
                            workSheet.Cells[rowcount, i + 1] = row[i].ToString();
                        
                    }
                }
                cellRange = workSheet.Range[workSheet.Cells[1, 1], workSheet.Cells[rowcount, tempDt.Columns.Count]];
                cellRange.EntireColumn.AutoFit();
            }
            catch (Exception)
            {
                MessageBox.Show("Failed to Export", "Error");
            }
        }
        public static void ExportToPdfMonthlypurchasereport(DataGrid grid)
        {

            try { 
            System.IO.Directory.CreateDirectory(System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\pos");


            IEnumerable itemsSource = grid.ItemsSource as IEnumerable;
            if (itemsSource != null && basicPos2.models.Purchases.AllPurchases.Count != 0)
            {
                PdfPTable table = new PdfPTable(grid.Columns.Count);
                Document doc = new Document(iTextSharp.text.PageSize.A4, 25, 25, 30, 30);
                PdfWriter writer = PdfWriter.GetInstance(doc, new System.IO.FileStream(pathfornarnia + "MonthlyPurchasesReport--" + todaydate + ".pdf", System.IO.FileMode.Create, FileAccess.ReadWrite));
                doc.Open();
                for (int j = 0; j < grid.Columns.Count; j++)
                {
                    table.AddCell(new Phrase(grid.Columns[j].Header.ToString()));
                }
                table.HeaderRows = 1;
                foreach (var item in itemsSource)
                {

                    DataGridRow row = grid.ItemContainerGenerator.ContainerFromItem(item) as DataGridRow;
                    if (row != null)
                    {
                        DataGridCellsPresenter presenter = FindVisualChild<DataGridCellsPresenter>(row);
                        for (int i = 0; i < grid.Columns.Count; ++i)
                        {
                            DataGridCell cell = (DataGridCell)presenter.ItemContainerGenerator.ContainerFromIndex(i);
                            TextBlock txt = cell.Content as TextBlock;
                            if (txt != null)
                            {

                                table.AddCell(new Phrase(txt.Text));
                            }
                            else
                            {
                                table.AddCell(new Phrase(""));
                            }
                        }
                    }
                }

                doc.Add(table);
                doc.Close();
                MessageBox.Show("Successfully exported to pdf", "Success");

            }
            else
            {
                MessageBox.Show("Cannot export empty form to pdf", "Error");
            }
            }
            catch
            {
                MessageBox.Show("Please close all instances of any PDF reader and try again", "Error");
            }
        }
        public static void Exporttocsvmonthlyexpensereport(DataGrid datagrid)
        {try { 
            datagrid.SelectAllCells();
            System.IO.Directory.CreateDirectory(System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\pos");

            datagrid.ClipboardCopyMode = DataGridClipboardCopyMode.IncludeHeader;
            ApplicationCommands.Copy.Execute(null, datagrid);

            datagrid.UnselectAllCells();
            string result = (string)System.Windows.Clipboard.GetData(System.Windows.DataFormats.CommaSeparatedValue);
            File.WriteAllText(pathfornarnia + "MonthlyExpensesReport--" + todaydate + ".csv", "", UnicodeEncoding.UTF8);

            File.AppendAllText(pathfornarnia + "MonthlyExpensesReport--" + todaydate + ".csv", result, UnicodeEncoding.UTF8);

            MessageBox.Show("Exported to csv successfully", "Success");
            }
            catch
            {
                MessageBox.Show("Please close all instances of any CSV reader and try again", "Error");
            }
        }
        public static void GenerateExcelmonthlyexpensereport(System.Data.DataTable DtIN)
        {
            try
            {
                excel = new Microsoft.Office.Interop.Excel.Application();
                excel.DisplayAlerts = false;
                excel.Visible = false;
                workBook = excel.Workbooks.Add(System.Type.Missing);
                workSheet = (Microsoft.Office.Interop.Excel.Worksheet)workBook.ActiveSheet;
                workSheet.Name = "Monthly Expense Report";
                System.Data.DataTable tempDt = DtIN;

                workSheet.Cells.Font.Size = 11;
                int rowcount = 1;
                for (int i = 1; i <= tempDt.Columns.Count-3; i++) //taking care of Headers.  
                {
                    workSheet.Cells[1, i] = tempDt.Columns[i-1].ColumnName;
                }
                foreach (System.Data.DataRow row in tempDt.Rows) //taking care of each Row  
                {
                    rowcount += 1;
                    for (int i = 0; i < tempDt.Columns.Count-3; i++) //taking care of each column  
                    {
                      
                            workSheet.Cells[rowcount, i + 1] = row[i].ToString();
                        
                    }
                }
                cellRange = workSheet.Range[workSheet.Cells[1, 1], workSheet.Cells[rowcount, tempDt.Columns.Count]];
                cellRange.EntireColumn.AutoFit();
            }
            catch (Exception)
            {
                MessageBox.Show("Failed to Export", "Error");
            }
        }
        public static void ExportToPdfMonthlyexpensereport(DataGrid grid)
        {
            try { 
            System.IO.Directory.CreateDirectory(System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\pos");
            IEnumerable itemsSource = grid.ItemsSource as IEnumerable;
            if (itemsSource != null && basicPos2.models.Purchases.AllPurchases.Count != 0)
            {
                PdfPTable table = new PdfPTable(grid.Columns.Count);
                Document doc = new Document(iTextSharp.text.PageSize.A4, 25, 25, 30, 30);
                PdfWriter writer = PdfWriter.GetInstance(doc, new System.IO.FileStream(pathfornarnia+ "MonthlyExpenseReport--" + todaydate + ".pdf", System.IO.FileMode.Create, FileAccess.ReadWrite));
                doc.Open();
                for (int j = 0; j < grid.Columns.Count; j++)
                {
                    table.AddCell(new Phrase(grid.Columns[j].Header.ToString()));
                }
                table.HeaderRows = 1;
                foreach (var item in itemsSource)
                {

                    DataGridRow row = grid.ItemContainerGenerator.ContainerFromItem(item) as DataGridRow;
                    if (row != null)
                    {
                        DataGridCellsPresenter presenter = FindVisualChild<DataGridCellsPresenter>(row);
                        for (int i = 0; i < grid.Columns.Count; ++i)
                        {
                            DataGridCell cell = (DataGridCell)presenter.ItemContainerGenerator.ContainerFromIndex(i);
                            TextBlock txt = cell.Content as TextBlock;
                            if (txt != null)
                            {

                                table.AddCell(new Phrase(txt.Text));
                            }
                            else
                            {
                                table.AddCell(new Phrase(""));
                            }
                        }
                    }
                }

                doc.Add(table);
                doc.Close();
                MessageBox.Show("Successfully exported to pdf", "Success");

            }
            else
            {
                MessageBox.Show("Cannot export empty form to pdf", "Error");
            }
            }
            catch
            {
                MessageBox.Show("Please close all instances of any PDF reader and try again", "Error");
            }
        }
        public static void Exporttocsvproductreport(DataGrid datagrid)
        {try { 
            datagrid.SelectAllCells();

            datagrid.ClipboardCopyMode = DataGridClipboardCopyMode.IncludeHeader;
            ApplicationCommands.Copy.Execute(null, datagrid);
            System.IO.Directory.CreateDirectory(System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\pos");

            datagrid.UnselectAllCells();
            string result = (string)System.Windows.Clipboard.GetData(System.Windows.DataFormats.CommaSeparatedValue);
            File.WriteAllText(pathfornarnia+ "ProductsReport--" + todaydate + ".csv", "", UnicodeEncoding.UTF8);

            File.AppendAllText(pathfornarnia+ "ProductsReport--" + todaydate + ".csv", result, UnicodeEncoding.UTF8);

            MessageBox.Show("Exported to csv successfully", "Success");
            }
            catch
            {
                MessageBox.Show("Please close all instances of any CSV reader and try again", "Error");
            }
        }
        public static void GenerateExcelproductreport(System.Data.DataTable DtIN)
        {
            try
            {
                excel = new Microsoft.Office.Interop.Excel.Application();
                excel.DisplayAlerts = false;
                excel.Visible = false;
                workBook = excel.Workbooks.Add(System.Type.Missing);
                workSheet = (Microsoft.Office.Interop.Excel.Worksheet)workBook.ActiveSheet;
                workSheet.Name = "Products Report";
                System.Data.DataTable tempDt = DtIN;

                workSheet.Cells.Font.Size = 11;
                int rowcount = 1;

                workSheet.Cells[1, 1] = "Name";
                workSheet.Cells[1, 2] = "Sold(Quantity)";
                workSheet.Cells[1, 3] = "Income";
                workSheet.Cells[1, 4] = "Cost";
                workSheet.Cells[1, 5] = "Profit";

                foreach (System.Data.DataRow row in tempDt.Rows) //taking care of each Row  
                {
                    rowcount += 1;
                    for (int i = 0; i < tempDt.Columns.Count-1; i++) //taking care of each column  
                    {
                        
                            workSheet.Cells[rowcount, i + 1] = row[i+1].ToString();
                        
                    }
                }
                cellRange = workSheet.Range[workSheet.Cells[1, 1], workSheet.Cells[rowcount, tempDt.Columns.Count]];
                cellRange.EntireColumn.AutoFit();
                workBook.SaveAs(System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"Exports\\productList.xlsx"), System.IO.FileMode.Create, FileAccess.ReadWrite);
            }
            catch (Exception)
            {
                MessageBox.Show("Failed to Export", "Error");
            }
        }
        public static void ExportToPdfproductreport(DataGrid grid)
        {
            try { 
            System.IO.Directory.CreateDirectory(System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\pos");

            IEnumerable itemsSource = grid.ItemsSource as IEnumerable;
            if (itemsSource != null && basicPos2.models.Purchases.AllPurchases.Count != 0)
            {
                PdfPTable table = new PdfPTable(grid.Columns.Count);
                Document doc = new Document(iTextSharp.text.PageSize.A4, 25, 25, 30, 30);
                PdfWriter writer = PdfWriter.GetInstance(doc, new System.IO.FileStream(pathfornarnia+ "ProductsReport--" + todaydate + ".pdf", System.IO.FileMode.Create, FileAccess.ReadWrite));
                doc.Open();
                for (int j = 0; j < grid.Columns.Count; j++)
                {
                    table.AddCell(new Phrase(grid.Columns[j].Header.ToString()));
                }
                table.HeaderRows = 1;
                foreach (var item in itemsSource)
                {

                    DataGridRow row = grid.ItemContainerGenerator.ContainerFromItem(item) as DataGridRow;
                    if (row != null)
                    {
                        DataGridCellsPresenter presenter = FindVisualChild<DataGridCellsPresenter>(row);
                        for (int i = 0; i < grid.Columns.Count; ++i)
                        {
                            DataGridCell cell = (DataGridCell)presenter.ItemContainerGenerator.ContainerFromIndex(i);
                            TextBlock txt = cell.Content as TextBlock;
                            if (txt != null)
                            {

                                table.AddCell(new Phrase(txt.Text));
                            }
                            else
                            {
                                table.AddCell(new Phrase(""));
                            }
                        }
                    }
                }

                doc.Add(table);
                doc.Close();
                MessageBox.Show("Successfully exported to pdf", "Success");

            }
            else
            {
                MessageBox.Show("Cannot export empty form to pdf", "Error");
            }
            }
            catch
            {
                MessageBox.Show("Please close all instances of any PDF reader and try again", "Error");
            }
        }
        public static BitmapImage ToImage(byte[] array)
        {
            BitmapImage m = new BitmapImage();
            m.BeginInit();
            m.StreamSource = new System.IO.MemoryStream(array);
            m.EndInit();
            return m;
        } 
    }

    public static class StringCipher
    {
        // This constant is used to determine the keysize of the encryption algorithm in bits.
        // We divide this by 8 within the code below to get the equivalent number of bytes.
        private const int Keysize = 256;

        // This constant determines the number of iterations for the password bytes generation function.
        private const int DerivationIterations = 1000;

        public static string Encrypt(string plainText, string passPhrase)
        {
            // Salt and IV is randomly generated each time, but is preprended to encrypted cipher text
            // so that the same Salt and IV values can be used when decrypting.  
            var saltStringBytes = Generate256BitsOfRandomEntropy();
            var ivStringBytes = Generate256BitsOfRandomEntropy();
            var plainTextBytes = Encoding.UTF8.GetBytes(plainText);
            using (var password = new Rfc2898DeriveBytes(passPhrase, saltStringBytes, DerivationIterations))
            {
                var keyBytes = password.GetBytes(Keysize / 8);
                using (var symmetricKey = new RijndaelManaged())
                {
                    symmetricKey.BlockSize = 256;
                    symmetricKey.Mode = CipherMode.CBC;
                    symmetricKey.Padding = PaddingMode.PKCS7;
                    using (var encryptor = symmetricKey.CreateEncryptor(keyBytes, ivStringBytes))
                    {
                        using (var memoryStream = new MemoryStream())
                        {
                            using (var cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write))
                            {
                                cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
                                cryptoStream.FlushFinalBlock();
                                // Create the final bytes as a concatenation of the random salt bytes, the random iv bytes and the cipher bytes.
                                var cipherTextBytes = saltStringBytes;
                                cipherTextBytes = cipherTextBytes.Concat(ivStringBytes).ToArray();
                                cipherTextBytes = cipherTextBytes.Concat(memoryStream.ToArray()).ToArray();
                                memoryStream.Close();
                                cryptoStream.Close();
                                return Convert.ToBase64String(cipherTextBytes);
                            }
                        }
                    }
                }
            }
        }

        public static string Decrypt(string cipherText, string passPhrase)
        {
            // Get the complete stream of bytes that represent:
            // [32 bytes of Salt] + [32 bytes of IV] + [n bytes of CipherText]
            var cipherTextBytesWithSaltAndIv = Convert.FromBase64String(cipherText);
            // Get the saltbytes by extracting the first 32 bytes from the supplied cipherText bytes.
            var saltStringBytes = cipherTextBytesWithSaltAndIv.Take(Keysize / 8).ToArray();
            // Get the IV bytes by extracting the next 32 bytes from the supplied cipherText bytes.
            var ivStringBytes = cipherTextBytesWithSaltAndIv.Skip(Keysize / 8).Take(Keysize / 8).ToArray();
            // Get the actual cipher text bytes by removing the first 64 bytes from the cipherText string.
            var cipherTextBytes = cipherTextBytesWithSaltAndIv.Skip((Keysize / 8) * 2).Take(cipherTextBytesWithSaltAndIv.Length - ((Keysize / 8) * 2)).ToArray();

            using (var password = new Rfc2898DeriveBytes(passPhrase, saltStringBytes, DerivationIterations))
            {
                var keyBytes = password.GetBytes(Keysize / 8);
                using (var symmetricKey = new RijndaelManaged())
                {
                    symmetricKey.BlockSize = 256;
                    symmetricKey.Mode = CipherMode.CBC;
                    symmetricKey.Padding = PaddingMode.PKCS7;
                    using (var decryptor = symmetricKey.CreateDecryptor(keyBytes, ivStringBytes))
                    {
                        using (var memoryStream = new MemoryStream(cipherTextBytes))
                        {
                            using (var cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read))
                            {
                                var plainTextBytes = new byte[cipherTextBytes.Length];
                                var decryptedByteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);
                                memoryStream.Close();
                                cryptoStream.Close();
                                return Encoding.UTF8.GetString(plainTextBytes, 0, decryptedByteCount);
                            }
                        }
                    }
                }
            }
        }

        private static byte[] Generate256BitsOfRandomEntropy()
        {
            var randomBytes = new byte[32]; // 32 Bytes will give us 256 bits.
            using (var rngCsp = new RNGCryptoServiceProvider())
            {
                // Fill the array with cryptographically secure random bytes.
                rngCsp.GetBytes(randomBytes);
            }
            return randomBytes;
        }
    }
}


