﻿using basicPos2.models;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace basicPos2
{
    /// <summary>
    /// Interaction logic for popupcatagory.xaml
    /// </summary>
    public partial class popupcatagory : Window
    {
        private ObservableCollection<Product> combogridpopulator;
        private ObservableCollection<Product> comboaddpopulator;
        private byte[] selectedphoto = null;
        private Catagories p;
        private string vis = "Visible";
        private Catagory populator;
        public popupcatagory(Catagories p, Catagory pop)
        {
            this.p = p;
            this.populator = pop;
            InitializeComponent();
            combogridpopulator = new ObservableCollection<Product>();
            
            comboaddpopulator = new ObservableCollection<Product>(Product.AllProducts);
         
            if (pop != null)
            {

                Name.Text = pop.Name;
                Code.Text = pop.Code;
                
                
               
              

            }
        }
        private void newguy_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            if (Name.Text.Equals(""))
            {
                Name.BorderBrush = Brushes.Red;
                MessageBox.Show("Please Enter a Name","Error");
            }else
            {
                if (Code.Text.Equals(""))
                {
                    Code.BorderBrush = Brushes.Red;
                    MessageBox.Show("Please Enter a Code","Error");

                }else
                {
                    if (populator == null)
                    {

                        Catagory.AddToAllCatagories(new Catagory { Vis = vis, Image = selectedphoto, Id = Catagory.AllCatagories.Count() + 1, Code = Code.Text, Name = Name.Text });
                        this.Close();
                    }
                    else
                    {
                        if (selectedphoto != null)
                        {
                            Catagory.editCategory(Catagory.AllCatagories.IndexOf(populator), new Catagory { Image = selectedphoto, Vis = vis, Id = populator.Id, Code = Code.Text, Name = Name.Text });
                            Product.ClearAllProducts();
                            Product.getFromDatabase();
                            this.Close();
                        }
                        else
                        {
                            Catagory.editCategory(Catagory.AllCatagories.IndexOf(populator), new Catagory { Id = populator.Id, Vis = populator.Vis, Image = populator.Image, Code = Code.Text, Name = Name.Text });
                            Product.ClearAllProducts();
                            Product.getFromDatabase();
                            this.Close();
                        }
                    }
                }
            }
            
        }

        private void AddImage_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog op = new OpenFileDialog();
            op.Title = "Select a picture";
            op.Filter = "All supported graphics|*.jpg;*.jpeg;*.png|" +
              "JPEG (*.jpg;*.jpeg)|*.jpg;*.jpeg|" +
              "Portable Network Graphic (*.png)|*.png";
            if (op.ShowDialog() == true)
            {
                selectedphoto = System.IO.File.ReadAllBytes(op.FileName);
                vis = "Hidden";
              
            }
        }

       

        

        private void Type_GiveFeedback(object sender, GiveFeedbackEventArgs e)
        {

        }

        private void Type_FocusableChanged(object sender, DependencyPropertyChangedEventArgs e)
        {

        }

        private void Type_Loaded(object sender, RoutedEventArgs e)
        {

        }

        

        private void comboaddproduct_KeyDown(object sender, KeyEventArgs e)
        {

        }


       

      
    }
}

