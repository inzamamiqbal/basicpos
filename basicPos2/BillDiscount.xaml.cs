﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using basicPos2.models;
namespace basicPos2
{
    /// <summary>
    /// Interaction logic for BillDiscount.xaml
    /// </summary>
    public partial class BillDiscount : Window
    {
        public BillDiscount()
        {
            InitializeComponent();
        }

        private void okBtn_Click(object sender, RoutedEventArgs e)
        {
            Sales.currentSale.OrderDiscount = float.Parse(billDiscountTxt.Text);
            this.Close();
        }

        private void cancelBtn_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
