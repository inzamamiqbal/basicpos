﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Microsoft.Win32;
using basicPos2.models;
using System.Collections.ObjectModel;
using System.IO;

namespace basicPos2
{
    /// <summary>
    /// Interaction logic for popupwindow.xaml
    /// </summary>
    public partial class popupwindow : Window
    {
        private ObservableCollection<Product> combogridpopulator;
        private ObservableCollection<Product> comboaddpopulator;
        private byte[] selectedphoto = null;
        private Products p;
        private Product populator;
        private string vis = "Visible";
        public popupwindow(Products p, Product pop)
        {
            this.p = p;
            this.populator = pop;
            InitializeComponent();
            combogridpopulator = new ObservableCollection<Product>();
         
            comboaddpopulator = new ObservableCollection<Product>(Product.AllProducts);
            comboaddproduct.ItemsSource = comboaddpopulator;
            comboproductdatagrid.ItemsSource = combogridpopulator;
            combocatagory.ItemsSource = Catagory.AllCatagories;
            catagorycombobox.ItemsSource = Catagory.AllCatagories;
            if (pop != null)
            {

                Name.Text = pop.Name;
                Code.Text = pop.Code;
                Cost.Text = pop.Cost.ToString();
                Type.Text = pop.Type;
                Price.Text = pop.Price.ToString();
                catagorycombobox.Text = ((Catagory)Catagory.AllCatagories.First(item => item.Id == pop.CatagoryId)).Name;
               /* if (pop.Type.Equals("Combo"))
                {
                    Product.getComboProducts(pop.Id,combogridpopulator);
                    ComboName.Text = pop.Name;
                    combocode.Text = pop.Code;
                    comboprice.Text = pop.Price.ToString();
                    combocatagory.Text = ((Catagory)Catagory.AllCatagories.First(item=>item.Id==pop.CatagoryId)).Name;
                    
                }*/
                
                Detail.Text = pop.Details;
                if (pop.Quantity != null)
                {
                    AlertQuantity.Text = pop.AlertQuantity.ToString();
                    Quantity.Text = pop.Quantity.ToString();
                }

            }
        }


        private void newguy_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            Name.BorderBrush = Brushes.Black;
            Code.BorderBrush = Brushes.Black;
            catagorycombobox.BorderBrush = Brushes.Black;
            AlertQuantity.BorderBrush = Brushes.Black;

            Quantity.BorderBrush = Brushes.Black;
            Cost.BorderBrush = Brushes.Black;
            Price.BorderBrush = Brushes.Black;
            if (Name.Text.Equals("")) {
                Name.BorderBrush = Brushes.Red;
                scroller.ScrollToTop();
                MessageBox.Show("Please fill in Name","Error");
            }else
            {
                if (Code.Text.Equals(""))
                {
                    Code.BorderBrush = Brushes.Red;
                    scroller.ScrollToTop();
                    MessageBox.Show("Please fill in Code", "Error");

                }else
                {
                    if (catagorycombobox.SelectedItem == null)
                    {
                        catagorycombobox.BorderBrush = Brushes.Red;
                        scroller.ScrollToTop();
                        MessageBox.Show("Please select a catagory", "Error");

                    }else
                    {
                        try { float.Parse(AlertQuantity.Text.ToString());
                            try { float.Parse(Quantity.Text.ToString());
                                try { float.Parse(Cost.Text.ToString());
                                    try
                                    {
                                        float.Parse(Price.Text.ToString());
                                        if (populator == null)
                                        {

                                            Product.AddToAllProduts(new Product { Vis = vis, Image = selectedphoto, Id = Product.AllProducts.Count() + 1, AlertQuantity = float.Parse(AlertQuantity.Text.ToString()), Code = Code.Text, Name = Name.Text, Type = Type.Text, CatagoryId = ((Catagory)catagorycombobox.SelectedItem).Id, Quantity = float.Parse(Quantity.Text.ToString()), Cost = float.Parse(Cost.Text.ToString()), Price = float.Parse(Price.Text.ToString()), Details = Detail.Text });
                                            this.Close();
                                        }
                                        else
                                        {
                                            if (selectedphoto != null)
                                            {
                                                Product.editProduct(Product.AllProducts.IndexOf(populator), new Product { Vis = vis, Image = selectedphoto, Id = populator.Id, AlertQuantity = float.Parse(AlertQuantity.Text.ToString()), Code = Code.Text, Name = Name.Text, Type = Type.Text, CatagoryId = ((Catagory)catagorycombobox.SelectedItem).Id, Quantity = float.Parse(Quantity.Text.ToString()), Cost = float.Parse(Cost.Text.ToString()), Price = float.Parse(Price.Text.ToString()), Details = Detail.Text });

                                                this.Close();
                                            }
                                            else
                                            {
                                                Product.editProduct(Product.AllProducts.IndexOf(populator), new Product { Id = populator.Id,Vis=populator.Vis,Image=populator.Image, AlertQuantity = float.Parse(AlertQuantity.Text.ToString()), Code = Code.Text, Name = Name.Text, Type = Type.Text, CatagoryId = ((Catagory)catagorycombobox.SelectedItem).Id, Quantity = float.Parse(Quantity.Text.ToString()), Cost = float.Parse(Cost.Text.ToString()), Price = float.Parse(Price.Text.ToString()), Details = Detail.Text });

                                                this.Close();

                                            }
                                        }

                                    }
                                    catch
                                    {
                                        Cost.BorderBrush = Brushes.Red;
                                        scroller.ScrollToTop();
                                        MessageBox.Show("Please enter a valid input", "Error");

                                    }

                                }
                                catch
                                {
                                    Cost.BorderBrush = Brushes.Red;
                                    scroller.ScrollToTop();
                                    MessageBox.Show("Please enter a valid input", "Error");

                                }

                            } catch {
                                Quantity.BorderBrush = Brushes.Red;
                                scroller.ScrollToTop();
                                MessageBox.Show("Please enter a valid input", "Error");

                            }


                        } catch
                        {
                            AlertQuantity.BorderBrush = Brushes.Red;
                            scroller.ScrollToTop();
                            MessageBox.Show("Please enter a valid input", "Error");

                        }
                    }
                }
            }
            
            

               
            
        }

        private void AddImage_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog op = new OpenFileDialog();
            op.Title = "Select a picture";
            op.Filter = "All supported graphics|*.jpg;*.jpeg;*.png|" +
              "JPEG (*.jpg;*.jpeg)|*.jpg;*.jpeg|" +
              "Portable Network Graphic (*.png)|*.png";
            if (op.ShowDialog() == true)
            {
                selectedphoto = File.ReadAllBytes(op.FileName);
                vis = "Hidden";
                label1.Content = selectedphoto.ToString();
            }
        }

        private void Type_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (mstandard != null && mcombo != null && Quantity != null && label11 != null && label8 != null && AlertQuantity != null)
            {
                if (Type.SelectedIndex == 0)
                {
                    mstandard.Visibility = Visibility.Visible;
                    mcombo.Visibility = Visibility.Hidden;
                    Quantity.Visibility = Visibility.Visible;

                    label11.Visibility = Visibility.Visible;
                    label8.Visibility = Visibility.Visible;
                    AlertQuantity.Visibility = Visibility.Visible;
                }
                else
                {
                    if (Type.SelectedIndex == 2)
                    {
                        mstandard.Visibility = Visibility.Visible;
                        mcombo.Visibility = Visibility.Hidden;
                        label11.Visibility = Visibility.Hidden;
                        label8.Visibility = Visibility.Hidden;
                        Quantity.Visibility = Visibility.Hidden;
                        AlertQuantity.Visibility = Visibility.Hidden;
                    }
                    else
                    {
                        if (Type.SelectedIndex == 1)
                        {
                            comboBoxn1.SelectedIndex = 1;
                            mstandard.Visibility = Visibility.Hidden;
                            mcombo.Visibility = Visibility.Visible;

                        }

                    }

                }
            }
        }

        private void buttonn_Click(object sender, RoutedEventArgs e)
        {
            ComboName.BorderBrush = Brushes.Black;
            combocode.BorderBrush = Brushes.Black;
            comboprice.BorderBrush = Brushes.Black;
            combocatagory.BorderBrush = Brushes.Black;
            comboaddproduct.BorderBrush = Brushes.Black;

            if (ComboName.Text.Equals(""))
            {
                ComboName.BorderBrush = Brushes.Red;
                scroller.ScrollToTop();
                MessageBox.Show("Please fill in Name", "Error");
            }
            else
            {
                if (combocode.Text.Equals(""))
                {
                    combocode.BorderBrush = Brushes.Red;
                    scroller.ScrollToTop();
                    MessageBox.Show("Please fill in Code", "Error");

                }
                else
                {
                    if (combocatagory.SelectedItem == null)
                    {
                        combocatagory.BorderBrush = Brushes.Red;
                        scroller.ScrollToTop();
                        MessageBox.Show("Please select a catagory", "Error");

                    }
                    else
                    {
                        if (combogridpopulator.Count == 0)
                        {
                            comboaddproduct.BorderBrush = Brushes.Red;
                            scroller.ScrollToTop();
                            MessageBox.Show("Please select add a product", "Error");
                        }
                        else
                        {
                            try
                            {
                                float.Parse(comboprice.Text.ToString());

                                float.Parse(Price.Text.ToString());
                                if (populator == null)
                                {
                                    label1.Content = selectedphoto;
                                    Product.addCombo(new Product { Vis = vis, Image = selectedphoto, Id = Product.AllProducts.Count + 1, Type = comboBoxn1.Text, CatagoryId = ((Catagory)combocatagory.SelectedItem).Id, Price = float.Parse(comboprice.Text), Code = combocode.Text, Name = ComboName.Text, Catagory = combocatagory.Text }, combogridpopulator);
                                    this.Close();
                                }
                                else
                                {
                                    if (selectedphoto != null)
                                    {
                                        Product.editCombo(populator, new Product { Vis = vis, Image = selectedphoto, Id = populator.Id, Type = comboBoxn1.Text, Price = float.Parse(comboprice.Text), CatagoryId = ((Catagory)catagorycombobox.SelectedItem).Id, Code = combocode.Text, Name = ComboName.Text, Catagory = combocatagory.Text }, combogridpopulator);
                                        this.Close();
                                    }
                                    else
                                    {
                                        Product.editCombo(populator, new Product { Id = populator.Id, Type = comboBoxn1.Text, Price = float.Parse(comboprice.Text), CatagoryId = ((Catagory)catagorycombobox.SelectedItem).Id, Code = combocode.Text, Name = ComboName.Text, Catagory = combocatagory.Text }, combogridpopulator);
                                        this.Close();

                                    }
                                }

                            }

                            catch
                            {
                                comboprice.BorderBrush = Brushes.Red;
                                scroller.ScrollToTop();
                                MessageBox.Show("Please enter a valid input", "Error");

                            }
                        }
                    }
                }
            }

           
        }

        private void Type_GiveFeedback(object sender, GiveFeedbackEventArgs e)
        {

        }

        private void Type_FocusableChanged(object sender, DependencyPropertyChangedEventArgs e)
        {

        }

        private void Type_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void comboBoxn1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (mstandard != null && mcombo != null && Quantity != null && label11 != null && label8 != null && AlertQuantity != null)
            {
                if (comboBoxn1.SelectedIndex == 0)
                {
                    Type.SelectedIndex = 0;
                    mstandard.Visibility = Visibility.Visible;
                    mcombo.Visibility = Visibility.Hidden;
                    Quantity.Visibility = Visibility.Visible;

                    label11.Visibility = Visibility.Visible;
                    label8.Visibility = Visibility.Visible;
                    AlertQuantity.Visibility = Visibility.Visible;
                }
                else
                {
                    if (comboBoxn1.SelectedIndex == 2)
                    {
                        Type.SelectedIndex = 2;
                        mstandard.Visibility = Visibility.Visible;
                        mcombo.Visibility = Visibility.Hidden;
                        label11.Visibility = Visibility.Hidden;
                        label8.Visibility = Visibility.Hidden;
                        Quantity.Visibility = Visibility.Hidden;
                        AlertQuantity.Visibility = Visibility.Hidden;
                    }
                    else
                    {
                        if (comboBoxn1.SelectedIndex == 1)
                        {
                            mstandard.Visibility = Visibility.Hidden;
                            mcombo.Visibility = Visibility.Visible;

                        }

                    }

                }
            }
        }

        private void comboaddproduct_KeyDown(object sender, KeyEventArgs e)
        {

        }

        private void comboaddproduct_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if ((Product)comboaddproduct.SelectedItem != null)
            {
                if (!combogridpopulator.Any(item => item.Code.Equals(((Product)comboaddproduct.SelectedItem).Code)))
                {
                    

                    combogridpopulator.Add(new Product { Name= ((Product)comboaddproduct.SelectedItem).ToString(), Code=((Product)comboaddproduct.SelectedItem).Code,Quantity=1 });
                }
            }
        }

        private void Quantity_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                try
                {
                    if (!(sender as TextBox).Text.ToString().Equals("") && (float.Parse((sender as TextBox).Text) != 0))
                    {

                        ((Product)comboproductdatagrid.SelectedItem).Quantity = float.Parse((sender as TextBox).Text);

                    }
                    else
                    {
                        MessageBox.Show("Please enter a valid number other than 0 for quantity");
                    }
                }
                catch (Exception)
                {
                    MessageBox.Show("Please enter a valid number other than 0 for quantity");

                }
            }
            else
            {

            }
        }

        private void Quantit_KeyDown(object sender, KeyEventArgs e)
        {
            //god i created this bymistake
        }

        private void delete_Click(object sender, RoutedEventArgs e)
        {
            combogridpopulator.Remove((Product)comboproductdatagrid.SelectedItem);
        }
        private static popupcatagory addProductsPop = null;
        private void button5_Click(object sender, RoutedEventArgs e)
        {
            if (addProductsPop == null || addProductsPop.IsLoaded == false)
            {
                addProductsPop = new popupcatagory(null, null);
                addProductsPop.Title = "Add Category";
                addProductsPop.Show();
            }
            else
            {
                addProductsPop.Activate();
            }
        }
    }
}
