﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace basicPos2
{
    /// <summary>
    /// Interaction logic for OpenRegister.xaml
    /// </summary>
    public partial class OpenRegister : UserControl
    {
        public OpenRegister()
        {
            InitializeComponent();
        }

        private void openRegisterBtn_Click(object sender, RoutedEventArgs e)
        {
            double cashInHand = 0;
            try
            {
                cashInHand = Convert.ToDouble(chashInHandTxt.Text);
            }
            catch (Exception)
            {
                cashInHand =0;
            }
            
            MySqlConnection connection = new MySqlConnection(RealUtility.dbString);

            try
            {

                connection.Open();
                MySqlCommand cmd;
                cmd = connection.CreateCommand();
                cmd.CommandText = "Insert into tec_registers(date,user_id,cash_in_hand,status,machine_name) Values ('" + DateTime.Now.ToString("yyyy-MM-dd HH:mm") + "','" + RealUtility.currentUserId + "','" + cashInHand + "','" + "open" +"','" +System.Environment.MachineName+"' )";
               
                cmd.ExecuteNonQuery();

                RealUtility.isRegisterOn = 1;
                ((MainWindow)Window.GetWindow(this)).showOrHidePos();


            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.ToString(), "Error");
            }

            finally
            {
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    connection.Close();
                }
            }
        }
    }
}
