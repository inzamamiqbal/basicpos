﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using basicPos2.models;

namespace basicPos2
{
    /// <summary>
    /// Interaction logic for AddCustomerPos.xaml
    /// </summary>
    public partial class AddCustomerPos : Window
    {
        public AddCustomerPos()
        {
            InitializeComponent();
        }

        private void addCustomerBtn_Click(object sender, RoutedEventArgs e)
        {
            if (nameTxt.Text.Length > 2)
            {
                Customer.AddToAllCustomers(new Customer
                {
                    Name = nameTxt.Text,
                    Phone = phoneTxt.Text,
                    Email = emailTxt.Text,
                    Cf1 = cf1Txt.Text,
                    Cf2 = cf2txt.Text
                });
                this.Close();
            }
            else
            {
                MessageBox.Show("Please insert a valid customer name", "Error");
            }
        }

        private void cancelBtn_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
