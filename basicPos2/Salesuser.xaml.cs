﻿using basicPos2.models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace basicPos2
{
    /// <summary>
    /// Interaction logic for Salesuser.xaml
    /// </summary>
    public partial class Salesuser : UserControl
    {
        public Salesuser()
        {
            InitializeComponent();
            produtsdataGrid.ItemsSource = Sales.AllSales;
            Total.Content = ((ObservableCollection<Sales>)produtsdataGrid.ItemsSource).Sum(item => item.Total);
            Discount.Content= ((ObservableCollection<Sales>)produtsdataGrid.ItemsSource).Sum(item => item.TotalDiscount);
            GrandTotal.Content = ((ObservableCollection<Sales>)produtsdataGrid.ItemsSource).Sum(item => item.GrandTotal);
            Paid.Content = ((ObservableCollection<Sales>)produtsdataGrid.ItemsSource).Sum(item => item.Paid);
        }
        
        private void button_Click(object sender, RoutedEventArgs e)
        {

        }



        private void dataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void dataGrid_Loaded(object sender, RoutedEventArgs e)
        {


        }
        private static viewPayments editpop = null;
        private void viewbtn_Click(object sender, RoutedEventArgs e)
        {
            if (editpop == null||editpop.IsLoaded == false)
            {
                editpop = new viewPayments(this, (Sales)produtsdataGrid.SelectedItem);
                editpop.Title = "View Payments";
                editpop.Show();
            }else
            {
                editpop.Activate();
            }
            

        }

        private void deletebtn_Click(object sender, RoutedEventArgs e)
        {

            Sales.DeleteFromAllSales((Sales)produtsdataGrid.SelectedItem);
            Total.Content = ((ObservableCollection<Sales>)produtsdataGrid.ItemsSource).Sum(item => item.Total);
            Discount.Content = ((ObservableCollection<Sales>)produtsdataGrid.ItemsSource).Sum(item => item.TotalDiscount);
            GrandTotal.Content = ((ObservableCollection<Sales>)produtsdataGrid.ItemsSource).Sum(item => item.GrandTotal);
            Paid.Content = ((ObservableCollection<Sales>)produtsdataGrid.ItemsSource).Sum(item => item.Paid);


        }
        private static AddPayment addp = null;
        private void editbtn_Click(object sender, RoutedEventArgs e)
        {
            if (addp == null||addp.IsLoaded == false)
            {
                addp = new AddPayment(this, (Sales)produtsdataGrid.SelectedItem, null, null);
                addp.Title = "Add Payment";
                addp.Show();
            }else
            {
                addp.Activate();
            }
        }
        private static opensaleswindow listopenbills=null;
        private void button5_Click(object sender, RoutedEventArgs e)
        {
            if (listopenbills == null||listopenbills.IsLoaded == false)
            {
                listopenbills = new opensaleswindow();
                listopenbills.Title = "Open Bills";
                listopenbills.Show();
            }else
            {
                listopenbills.Activate();
            }

        }
        private void button3_Click(object sender, RoutedEventArgs e)
        {
            RealUtility.ExportToPdfSales(produtsdataGrid);
        }




        private void button2_Click(object sender, RoutedEventArgs e)
        {
            RealUtility.Exporttocsvsales(produtsdataGrid);
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            try { 
            System.IO.Directory.CreateDirectory(System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\pos");
            RealUtility.GenerateExcelSales(Sales.AllSales.ToDataTable());
            RealUtility.workBook.SaveAs(RealUtility.pathfornarnia+ "SalesList--" + RealUtility.todaydate + ".xls", System.IO.FileMode.Create, FileAccess.ReadWrite);
            RealUtility.workBook.Close();
            RealUtility.excel.Quit();

            MessageBox.Show("Successfuly Exported as Excel File", "Success");

            }
            catch
            {
                MessageBox.Show("Please close all instances of excel and try again", "Error");
            }
        }



        private void button6_Click(object sender, RoutedEventArgs e)
        {
            RealUtility.ImportFromExcelcategories();
        }

        private void button4_Click(object sender, RoutedEventArgs e)
        {


            (sender as System.Windows.Controls.Button).ContextMenu.IsEnabled = true;
            (sender as System.Windows.Controls.Button).ContextMenu.PlacementTarget = (sender as System.Windows.Controls.Button);
            (sender as System.Windows.Controls.Button).ContextMenu.Placement = System.Windows.Controls.Primitives.PlacementMode.Bottom;
            (sender as System.Windows.Controls.Button).ContextMenu.IsOpen = true;
        }

        private void textBox_KeyDown(object sender, KeyEventArgs e)
        {

            IEnumerable<Sales> backup = new ObservableCollection<Sales>((IEnumerable<Sales>)produtsdataGrid.ItemsSource);
            
            if (e.Key == Key.Return)
            {
                if (!textBox.Text.ToString().Equals(""))
                {

                    IEnumerable<Sales> temp = new ObservableCollection<Sales>();
                    temp = ((IEnumerable<Sales>)produtsdataGrid.ItemsSource).Where(item => item.CustomerName.ToLower().Contains(textBox.Text.ToLower()));
                    produtsdataGrid.ItemsSource = null;
                    produtsdataGrid.ItemsSource = temp;
                    produtsdataGrid.Items.Refresh();
                    Total.Content = ((IEnumerable<Sales>)produtsdataGrid.ItemsSource).Sum(item => item.Total);
                    Discount.Content = ((IEnumerable<Sales>)produtsdataGrid.ItemsSource).Sum(item => item.TotalDiscount);
                    GrandTotal.Content = ((IEnumerable<Sales>)produtsdataGrid.ItemsSource).Sum(item => item.GrandTotal);
                    Paid.Content = ((IEnumerable<Sales>)produtsdataGrid.ItemsSource).Sum(item => item.Paid);

                }
                else
                {
                    if (textBox.Text.Equals("") && Datesearch.Text.Equals("[Date]"))
                    {
                        textBox.Text = "[Customer]";
                        produtsdataGrid.ItemsSource = Sales.AllSales;
                        produtsdataGrid.Items.Refresh();

                        Total.Content = ((IEnumerable<Sales>)produtsdataGrid.ItemsSource).Sum(item => item.Total);
                        Discount.Content = ((IEnumerable<Sales>)produtsdataGrid.ItemsSource).Sum(item => item.TotalDiscount);
                        GrandTotal.Content = ((IEnumerable<Sales>)produtsdataGrid.ItemsSource).Sum(item => item.GrandTotal);
                        Paid.Content = ((IEnumerable<Sales>)produtsdataGrid.ItemsSource).Sum(item => item.Paid);

                    }
                    else
                    {
                        textBox.Text = "[Customer]";
                        produtsdataGrid.ItemsSource = backup;
                        produtsdataGrid.Items.Refresh();

                        Total.Content = ((IEnumerable<Sales>)produtsdataGrid.ItemsSource).Sum(item => item.Total);
                        Discount.Content = ((IEnumerable<Sales>)produtsdataGrid.ItemsSource).Sum(item => item.TotalDiscount);
                        GrandTotal.Content = ((IEnumerable<Sales>)produtsdataGrid.ItemsSource).Sum(item => item.GrandTotal);
                        Paid.Content = ((IEnumerable<Sales>)produtsdataGrid.ItemsSource).Sum(item => item.Paid);

                    }
                }
            }
        }

        private void textBox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        
        private void SelectAddress(object sender, RoutedEventArgs e)

        {

            System.Windows.Controls.TextBox tb = (sender as System.Windows.Controls.TextBox);

            if (tb != null)

            {

                tb.SelectAll();

            }

        }



        private void SelectivelyIgnoreMouseButton(object sender,

            MouseButtonEventArgs e)

        {

            System.Windows.Controls.TextBox tb = (sender as System.Windows.Controls.TextBox);

            if (tb != null)

            {

                if (!tb.IsKeyboardFocusWithin)

                {

                    e.Handled = true;

                    tb.Focus();

                }

            }

        }

       
        private void Datesearch_KeyDown(object sender, KeyEventArgs e)
        {
            IEnumerable<Sales> backup = new ObservableCollection<Sales>((IEnumerable<Sales>)this.produtsdataGrid.ItemsSource);

            if (e.Key == Key.Return)
            {
                if (!Datesearch.Text.ToString().Equals(""))
                {

                    IEnumerable<Sales> temp = new ObservableCollection<Sales>();
                    temp = Sales.AllSales.Where(item => item.Date.ToLower().Contains(Datesearch.Text.ToString().ToLower()));
                    produtsdataGrid.ItemsSource = null;
                    produtsdataGrid.ItemsSource = temp;
                    produtsdataGrid.Items.Refresh();

                    Total.Content = ((IEnumerable<Sales>)produtsdataGrid.ItemsSource).Sum(item => item.Total);
                    Discount.Content = ((IEnumerable<Sales>)produtsdataGrid.ItemsSource).Sum(item => item.TotalDiscount);
                    GrandTotal.Content = ((IEnumerable<Sales>)produtsdataGrid.ItemsSource).Sum(item => item.GrandTotal);
                    Paid.Content = ((IEnumerable<Sales>)produtsdataGrid.ItemsSource).Sum(item => item.Paid);

                }
                else
                {
                    if (textBox.Text.Equals("[Customer]") && Datesearch.Text.Equals("") && StatusSearch.Text.Equals("[Status]"))
                    {
                        Datesearch.Text = "[Date]";
                        produtsdataGrid.ItemsSource = Sales.AllSales;
                        produtsdataGrid.Items.Refresh();

                        Total.Content = ((IEnumerable<Sales>)produtsdataGrid.ItemsSource).Sum(item => item.Total);
                        Discount.Content = ((IEnumerable<Sales>)produtsdataGrid.ItemsSource).Sum(item => item.TotalDiscount);
                        GrandTotal.Content = ((IEnumerable<Sales>)produtsdataGrid.ItemsSource).Sum(item => item.GrandTotal);
                        Paid.Content = ((IEnumerable<Sales>)produtsdataGrid.ItemsSource).Sum(item => item.Paid);

                    }
                    else
                    {
                        Datesearch.Text = "[Date]";
                        produtsdataGrid.ItemsSource = backup;
                        produtsdataGrid.Items.Refresh();

                        Total.Content = ((IEnumerable<Sales>)produtsdataGrid.ItemsSource).Sum(item => item.Total);
                        Discount.Content = ((IEnumerable<Sales>)produtsdataGrid.ItemsSource).Sum(item => item.TotalDiscount);
                        GrandTotal.Content = ((IEnumerable<Sales>)produtsdataGrid.ItemsSource).Sum(item => item.GrandTotal);
                        Paid.Content = ((IEnumerable<Sales>)produtsdataGrid.ItemsSource).Sum(item => item.Paid);

                    }
                }
            }
        }

        private void label7_Copy2_KeyDown(object sender, KeyEventArgs e)
        {
            
        }

        private void StatusSearch_KeyDown(object sender, KeyEventArgs e)
        {
            IEnumerable<Sales> backup = new ObservableCollection<Sales>((IEnumerable<Sales>)produtsdataGrid.ItemsSource);

            if (e.Key == Key.Return)
            {
                if (!StatusSearch.Text.ToString().Equals(""))
                {

                    IEnumerable<Sales> temp = new ObservableCollection<Sales>();
                    temp = Sales.AllSales.Where(item => item.Status.ToString().Contains(StatusSearch.Text.ToString().ToLower()));
                    produtsdataGrid.ItemsSource = null;
                    produtsdataGrid.ItemsSource = temp;
                    produtsdataGrid.Items.Refresh();

                    Total.Content = ((IEnumerable<Sales>)produtsdataGrid.ItemsSource).Sum(item => item.Total);
                    Discount.Content = ((IEnumerable<Sales>)produtsdataGrid.ItemsSource).Sum(item => item.TotalDiscount);
                    GrandTotal.Content = ((IEnumerable<Sales>)produtsdataGrid.ItemsSource).Sum(item => item.GrandTotal);
                    Paid.Content = ((IEnumerable<Sales>)produtsdataGrid.ItemsSource).Sum(item => item.Paid);

                }
                else
                {
                    if (textBox.Text.Equals("[Customer]") && Datesearch.Text.Equals("[Date]") && StatusSearch.Text.Equals("") )
                    {
                        StatusSearch.Text = "[Status]";
                        produtsdataGrid.ItemsSource = Sales.AllSales;
                        produtsdataGrid.Items.Refresh();

                        Total.Content = ((IEnumerable<Sales>)produtsdataGrid.ItemsSource).Sum(item => item.Total);
                        Discount.Content = ((IEnumerable<Sales>)produtsdataGrid.ItemsSource).Sum(item => item.TotalDiscount);
                        GrandTotal.Content = ((IEnumerable<Sales>)produtsdataGrid.ItemsSource).Sum(item => item.GrandTotal);
                        Paid.Content = ((IEnumerable<Sales>)produtsdataGrid.ItemsSource).Sum(item => item.Paid);

                    }
                    else
                    {
                        StatusSearch.Text = "[Status]";
                        produtsdataGrid.ItemsSource = backup;
                        produtsdataGrid.Items.Refresh();

                        Total.Content = ((IEnumerable<Sales>)produtsdataGrid.ItemsSource).Sum(item => item.Total);
                        Discount.Content = ((IEnumerable<Sales>)produtsdataGrid.ItemsSource).Sum(item => item.TotalDiscount);
                        GrandTotal.Content = ((IEnumerable<Sales>)produtsdataGrid.ItemsSource).Sum(item => item.GrandTotal);
                        Paid.Content = ((IEnumerable<Sales>)produtsdataGrid.ItemsSource).Sum(item => item.Paid);

                    }
                }
            }

        }

        private void textBox_LostFocus(object sender, RoutedEventArgs e)
        {

        }

        private void SelectAddress(object sender, MouseButtonEventArgs e)
        {

        }

        private void textBox_MouseUp(object sender, MouseButtonEventArgs e)
        {

        }

        private void produtsdataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Bill bill = new Bill(null, false, (Sales)produtsdataGrid.SelectedItem);
            bill.ShowDialog();

        }
    }
    /* public class item :items
     {





         }
     public class item2 :items
     {

         public String Image { set; get; }



     }
     public class items {
         public BitmapImage Image { set; get; }
         public String Code { set; get; }
         public string Name { set; get; }
         public String Type { set; get; }
         public string Catagory { set; get; }
         public String Quantity { set; get; }
         public string Cost { set; get; }
         public string AlertQuantity { set; get; }

         public String Price { set; get; }
         public String Description { set; get; }
     }*/

}
