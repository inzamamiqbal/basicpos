﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace basicPos2
{
    /// <summary>
    /// Interaction logic for ViewEditDelete.xaml
    /// </summary>
    /// 
    

    public partial class ViewEditDelete : UserControl
    {   private static ViewEditDelete _instance;
    
        public static ViewEditDelete Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new ViewEditDelete();
                }
                return _instance;
            }

        }

        public ViewEditDelete()
        {
            InitializeComponent();
        }
    }
}
