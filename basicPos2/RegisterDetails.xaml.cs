﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using basicPos2.models;

namespace basicPos2
{
    /// <summary>
    /// Interaction logic for RegisterDetails.xaml
    /// </summary>
    public partial class RegisterDetails : UserControl
    {
        public RegisterDetails()
        {
            InitializeComponent();
            cashInHandLbl.Content = Register.currentRegister.CashInHand;
            totalExpenseLbl.Content = Register.currentRegister.TotalExpenses;
            totalSalesLbl.Content = Register.currentRegister.CashSalesTotal+Register.currentRegister.NoOfCheques;
            chasSalesLbl.Content = Register.currentRegister.CashSalesTotal;
            chequeSalesLbl.Content = Register.currentRegister.NoOfCheques;
            totalCashLbl.Content = Register.currentRegister.CashInHand + Register.currentRegister.CashSalesTotal - Register.currentRegister.TotalExpenses;

        }
    }
}
